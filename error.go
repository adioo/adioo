package adioo

// General errors.
const (
	ErrUnauthorized          = Error("unauthorized")
	ErrInternal              = Error("internal error")
	ErrNotFound              = Error("resource not found")
	ErrBadRequest            = Error("bad request")
	ErrInvalidJSON           = Error("invalid json")
	ErrInvalidJWT            = Error("invalid token")
	ErrMerchantNotIntegrated = Error("merchant not intergrated error")
	ErrProjectNameTaken      = Error("project name must be unique")
	ErrFolderNameTaken       = Error("folder name must be unique")
)

// Error represents a Vano error.
type Error string

// Error returns the error message.
func (e Error) Error() string {
	return string(e)
}
