package adioo

import (
	"strings"
	"time"
)

// EmployeeService is
type EmployeeService interface {
	Authenticate(email, password string) (*Employee, error)
	Create(n *NewEmployee) (*Employee, error)
	Update(u *UpdateEmployee) (*Employee, error)
	ListCompanyEmployees(companyID, sinceID string, limit int) ([]*Employee, error)
	Employee(id string) (*Employee, error)
	EmployeePermissions(id string) ([]*Permission, error)
	UpdateJTI(employeeID, jti string) error
}

// Permission is
type Permission struct {
	ID        string
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time

	Employees []*Employee `json:"-" pg:",many2many:employee_permission"`
}

// Employee is
type Employee struct {
	ID          string        `json:"id"`
	CompanyID   string        `json:"company_id"`
	FirstName   string        `json:"first_name"`
	LastName    string        `json:"last_name"`
	Email       string        `json:"email"`
	PhoneNumber string        `json:"phone_number"`
	Password    string        `json:"password"`
	AvatarURL   string        `json:"avatar_url"`
	JTI         string        `json:"-"`
	CreatedAt   time.Time     `json:"created_at"`
	UpdatedAt   time.Time     `json:"updated_at"`
	Permissions []*Permission `json:"permissions" pg:",many2many:employee_permission"`
}

// UpdateEmployee is
type UpdateEmployee struct {
	ID          string   `json:"-"`
	FirstName   string   `json:"first_name"`
	LastName    string   `json:"last_name"`
	Email       string   `json:"email"`
	PhoneNumber string   `json:"phone_number"`
	Permissions []string `json:"permissions"`
	AvatarURL   string   `json:"avatar_url"`
}

// NewEmployee is
type NewEmployee struct {
	CompanyID   string   `json:"company_id"`
	FirstName   string   `json:"first_name"`
	LastName    string   `json:"last_name"`
	Email       string   `json:"email"`
	PhoneNumber string   `json:"phone_number"`
	Password    string   `json:"password"`
	Permissions []string `json:"permissions"`
	AvatarURL   string   `json:"avatar_url"`
}

// Sanitize is
func (e *NewEmployee) Sanitize() {
	e.FirstName = strings.ToLower(e.FirstName)
	e.LastName = strings.ToLower(e.LastName)
}
