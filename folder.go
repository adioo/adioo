package adioo

import "time"

//FolderService is
type FolderService interface {
	Create(n *NewFolder) (*Folder, error)
	ListProjectFolders(projectID string) ([]*Folder, error)
	Folder(id string) (*Folder, error)
}

// Folder is
type Folder struct {
	ID          string    `json:"id"`
	ProjectID   string    `json:"project_id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Photos      []*Photo  `json:"photos" pg:",many2many:folder_photo,joinFK:photo"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// FolderPhoto is
type FolderPhoto struct {
	TableName struct{} `sql:"folder_photo"`
	FolderID  string
	PhotoID   string
}

// NewFolder is
type NewFolder struct {
	ProjectID   string `json:"project_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
