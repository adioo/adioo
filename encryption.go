package adioo

// EncryptionService is
type EncryptionService interface {
	EncryptURL(url string) (string, error)
	DecryptURL(url string) (string, error)
	DecryptURLs(urls []string) ([]string, error)
}
