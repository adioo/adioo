package adioo

import "time"

//PhotoService is
type PhotoService interface {
	Create(n *NewPhoto) (*Photo, error)
	ListProjectPhotos(projectID string) ([]*Photo, error)
	RemovePhoto(id string) error
	AddFolderPhoto(folderID, photoID string) error
	RemoveFolderPhoto(folderID, photoID string) error
	Photo(id string) (*Photo, error)
}

// NewPhoto is
type NewPhoto struct {
	ProjectID    string `json:"project_id"`
	URL          string `json:"url"`
	ThumbnailURL string `json:"thumbnail_url"`
}

// Photo is
type Photo struct {
	ID           string    `json:"id"`
	ProjectID    string    `json:"project_id"`
	URL          string    `json:"url"`
	ThumbnailURL string    `json:"-"`
	Folders      []*Folder `json:"-" pg:",many2many:folder_photo"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}
