package middleware

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	"gitlab.com/adioo/adioo/http/response"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/adioo/adioo"
)

// Auth is
type Auth struct {
	EmployeeStorage  adioo.EmployeeService
	BlacklistService adioo.BlacklistStorage
}

// TokenContextKey is
type TokenContextKey string

// GetReqToken is
func GetReqToken(r *http.Request) *jwt.Token {
	return r.Context().Value(TokenContextKey("token")).(*jwt.Token)
}

// Authentication is the authentication middleware
func (m *Auth) Authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		jwtToken, err := extractToken(r)
		if err != nil {
			response.WithError(w, err, http.StatusUnauthorized, GetReqID(r))
			return
		}

		token, err := parseToken(jwtToken)
		if err != nil {
			response.WithError(w, err, http.StatusUnauthorized, GetReqID(r))
			return
		}

		if err := m.checkJTI(token); err != nil {
			response.WithError(w, err, http.StatusUnauthorized, GetReqID(r))
			return
		}

		k := TokenContextKey("token")
		ctx := context.WithValue(r.Context(), k, token)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Authorization authorize specfied roles or authenticated user to access wrapped handler
func (m *Auth) Authorization(next http.Handler, scopes ...string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if len(scopes) == 0 {
			next.ServeHTTP(w, r)
		}

		token := GetReqToken(r)
		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			response.WithError(w, adioo.ErrInvalidJWT, http.StatusBadRequest, GetReqID(r))
			return
		}
		employeeID := claims["sub"].(string)

		p, err := m.EmployeeStorage.EmployeePermissions(employeeID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, GetReqID(r))
			return
		}

		var permissions []string
		for _, per := range p {
			permissions = append(permissions, per.Name)
		}

		for _, s := range scopes {
			if !adioo.StringInSlice(s, permissions) {
				response.WithError(w, errors.New("insufficient permissions for request"), http.StatusForbidden, GetReqID(r))
				return
			}
		}

		next.ServeHTTP(w, r)
	})
}

func (m *Auth) checkJTI(token *jwt.Token) error {
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return adioo.ErrInvalidJWT
	}
	jti := claims["jti"].(string)

	ok, err := m.BlacklistService.CheckJTI(jti)
	if err != nil {
		return err
	}
	if !ok {
		return errors.New("token is no longer valid")
	}
	return nil
}

func parseToken(jwtToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		// Valid alg is what we expect
		if token.Method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_TOKEN")), nil
	})
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("Invalid jwt token")
	}

	return token, nil
}

func extractToken(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", errors.New("Authorization header missing")
	}

	// TODO: Make this a bit more robust, parsing-wise
	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		return "", errors.New("Authorization header format must be Bearer {token}")
	}

	jwtToken := authHeaderParts[1]

	return jwtToken, nil
}
