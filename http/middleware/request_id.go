package middleware

import (
	"context"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

// reqIDContextKey is
type reqIDContextKey string

// GetReqID is
func GetReqID(r *http.Request) string {
	return r.Context().Value(reqIDContextKey("req_id")).(string)
}

// GenerateRequestID creates a id for each request coming in. It then passes it down via context
func GenerateRequestID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		k := reqIDContextKey("req_id")
		ctx := context.WithValue(r.Context(), k, uuid.NewV4().String())
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
