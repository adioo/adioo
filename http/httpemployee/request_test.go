package httpemployee

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"
)

func TestCreateEmployeeRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *createEmployeeRequest
		expected bool
	}{
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, true},
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909", Permissions: []string{"invalid"}}, false},
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909", AvatarURL: "invalid"}, false},
		{&createEmployeeRequest{CompanyID: "invaliduuid", FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", PhoneNumber: "1234560909"}, false},
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: "fn", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&createEmployeeRequest{CompanyID: mock.TestID, LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&createEmployeeRequest{FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "invalid", PhoneNumber: "1234560909"}, false},
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1"}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, result, test.expected)
	}
}

func TestUpdateEmployeeRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *updateEmployeeRequest
		expected bool
	}{
		{&updateEmployeeRequest{ID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, true},
		{&updateEmployeeRequest{ID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909", Permissions: []string{"invalid"}}, false},
		{&updateEmployeeRequest{ID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909", AvatarURL: "invalid"}, false},
		{&updateEmployeeRequest{ID: "invaliduuid", FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&updateEmployeeRequest{ID: mock.TestID, FirstName: "fn", LastName: "ln", PhoneNumber: "1234560909"}, false},
		{&updateEmployeeRequest{ID: mock.TestID, FirstName: "fn", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&updateEmployeeRequest{ID: mock.TestID, LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&updateEmployeeRequest{FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&updateEmployeeRequest{ID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "invalid", PhoneNumber: "1234560909"}, false},
		{&updateEmployeeRequest{ID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1"}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, result, test.expected)
	}
}
