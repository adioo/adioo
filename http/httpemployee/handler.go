package httpemployee

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	EmployeeStorage adioo.EmployeeService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(s adioo.EmployeeService) *Handler {
	return &Handler{
		Router:          mux.NewRouter(),
		EmployeeStorage: s,
	}
}
