package httpemployee

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badEmployeeRequest struct {
	FirstName int `json:"first_name"`
}

func TestHandleCreateEmployee(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&createEmployeeRequest{CompanyID: mock.TestID, FirstName: mock.TestCustomerName, LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, http.StatusCreated},
		{&createEmployeeRequest{CompanyID: mock.TestID}, http.StatusBadRequest},
		{&badEmployeeRequest{FirstName: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", "/employees", bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/employees", middleware.GenerateRequestID(testHandler.HandleCreateEmployee()))

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}

func TestGetEmployee(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invaliduuid", http.StatusBadRequest},
		{mock.WrongTestID, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/employees/%s", test.id)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/employees/{id}", middleware.GenerateRequestID(testHandler.HandleGetEmployee())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}
}

func TestUpdateEmployee(t *testing.T) {
	tests := []struct {
		payload  interface{}
		id       string
		respCode int
	}{
		{&updateEmployeeRequest{FirstName: mock.TestCustomerName, LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, mock.TestID, http.StatusOK},
		{&updateEmployeeRequest{}, mock.TestID, http.StatusBadRequest},
		{&badEmployeeRequest{FirstName: 1}, mock.TestID, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/employees/%s", test.id)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/employees/{id}", middleware.GenerateRequestID(testHandler.HandleUpdateEmployee())).Methods("PUT")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}
}

func TestHandleListCompanyEmployees(t *testing.T) {
	tests := []struct {
		id       string
		count    string
		respCode int
	}{
		{mock.TestID, "", http.StatusOK},
		{mock.TestID, "20", http.StatusOK},
		{"invalid uuid", "", http.StatusBadRequest},
		{mock.WrongTestID, "", http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/companies/%s/employees", test.id)
		if test.count != "" {
			url = fmt.Sprintf("/companies/%s/employees?count=%s", test.id, test.count)
		}
		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies/{company_id}/employees", middleware.GenerateRequestID(testHandler.HandleListCompanyEmployees())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}
