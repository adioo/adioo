package httpemployee

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type sanitizedEmployee struct {
	ID          string                      `json:"id"`
	FirstName   string                      `json:"first_name"`
	LastName    string                      `json:"last_name"`
	Email       string                      `json:"email"`
	PhoneNumber string                      `json:"phone_number"`
	Permissions []string                    `json:"permissions"`
	AvatarURL   string                      `json:"avatar_url"`
	Company     *response.RelationshipData  `json:"company"`
	Events      *response.RelationshipsData `json:"events"`
	CreatedAt   time.Time                   `json:"created_at"`
	UpdatedAt   time.Time                   `json:"updated_at"`
}

type employeeResponse struct {
	Data *sanitizedEmployee `json:"data"`
}

type employeesResponse struct {
	response.PaginationData
	Data []*sanitizedEmployee `json:"data"`
}

func sanitizeEmployee(p *adioo.Employee) *sanitizedEmployee {
	permissions := []string{}
	for _, per := range p.Permissions {
		permissions = append(permissions, per.Name)
	}

	return &sanitizedEmployee{
		ID: p.ID,
		Company: &response.RelationshipData{
			ID:   p.CompanyID,
			HREF: fmt.Sprintf("/companies/%s", p.CompanyID),
		},
		FirstName:   strings.Title(p.FirstName),
		LastName:    strings.Title(p.LastName),
		Email:       p.Email,
		PhoneNumber: p.PhoneNumber,
		Permissions: permissions,
		AvatarURL:   p.AvatarURL,
		Events: &response.RelationshipsData{
			HREF: fmt.Sprintf("/employees/%s/events", p.ID),
		},
		CreatedAt: p.CreatedAt,
		UpdatedAt: p.UpdatedAt,
	}
}

func sanitizeEmployees(s []*adioo.Employee) []*sanitizedEmployee {
	ss := make([]*sanitizedEmployee, len(s))
	for x, u := range s {
		nu := sanitizeEmployee(u)
		ss[x] = nu
	}

	return ss
}
