package httpemployee

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreateEmployee is
func (h *Handler) HandleCreateEmployee() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createEmployeeRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewEmployee{
			CompanyID:   payload.CompanyID,
			FirstName:   payload.FirstName,
			LastName:    payload.LastName,
			Email:       payload.Email,
			PhoneNumber: payload.PhoneNumber,
			Password:    payload.Password,
			Permissions: payload.Permissions,
			AvatarURL:   payload.AvatarURL,
		}

		e, err := h.EmployeeStorage.Create(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := employeeResponse{
			Data: sanitizeEmployee(e),
		}

		response.Created(w, resp)
	})
}

// HandleGetEmployee is
func (h *Handler) HandleGetEmployee() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		e, err := h.EmployeeStorage.Employee(id)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := employeeResponse{
			Data: sanitizeEmployee(e),
		}

		response.OK(w, resp)
	})
}

// HandleListCompanyEmployees is
func (h *Handler) HandleListCompanyEmployees() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		companyID := mux.Vars(r)["company_id"]
		if !govalidator.IsUUIDv4(companyID) {
			response.WithError(w, errors.New("invalid company id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		q := r.URL.Query()
		limit := 20
		if q.Get("count") != "" {
			icount, err := strconv.Atoi(q.Get("count"))
			if err != nil {
				response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
				return
			}
			limit = icount
		}
		sinceID := q.Get("since_id")

		e, err := h.EmployeeStorage.ListCompanyEmployees(companyID, sinceID, limit)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := employeesResponse{
			Data: sanitizeEmployees(e),
		}
		eLen := len(e)
		resp.Count = limit
		if eLen < limit {
			resp.Count = eLen
		}
		if eLen == limit {
			resp.SinceID = e[eLen-1].ID
			resp.Links.Next = fmt.Sprintf("/companies/%s/employees?since_id=%s", companyID, resp.SinceID)
		}
		resp.Links.Self = r.URL.String()

		response.OK(w, resp)
	})
}

// HandleUpdateEmployee is
func (h *Handler) HandleUpdateEmployee() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(updateEmployeeRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}
		payload.ID = mux.Vars(r)["id"]

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.UpdateEmployee{
			ID:          payload.ID,
			FirstName:   payload.FirstName,
			LastName:    payload.LastName,
			Email:       payload.Email,
			PhoneNumber: payload.PhoneNumber,
			Permissions: payload.Permissions,
			AvatarURL:   payload.AvatarURL,
		}

		e, err := h.EmployeeStorage.Update(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := employeeResponse{
			Data: sanitizeEmployee(e),
		}

		response.OK(w, resp)
	})
}
