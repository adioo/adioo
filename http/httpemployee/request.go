package httpemployee

import (
	"errors"
	"fmt"
	"net/url"

	"github.com/asaskevich/govalidator"
	"gitlab.com/adioo/adioo"
)

type createEmployeeRequest struct {
	CompanyID   string   `json:"company_id"`
	FirstName   string   `json:"first_name"`
	LastName    string   `json:"last_name"`
	Email       string   `json:"email"`
	PhoneNumber string   `json:"phone_number"`
	Password    string   `json:"password"`
	Permissions []string `json:"permissions"`
	AvatarURL   string   `json:"avatar_url"`
}

func (r *createEmployeeRequest) Validate() error {
	switch {
	case r.CompanyID == "":
		return errors.New("company_id is required")
	case !govalidator.IsUUIDv4(r.CompanyID):
		return errors.New("company id is invalid")
	case r.FirstName == "":
		return errors.New("first_name is required")
	case r.LastName == "":
		return errors.New("last_name is required")
	case r.Email == "":
		return errors.New("email is required")
	case !govalidator.IsEmail(r.Email):
		return errors.New("invalid email")
	}
	if r.PhoneNumber != "" {
		if len(r.PhoneNumber) != 10 {
			return errors.New("phone number is invalid. Only provide numeric characters. Ex. 9544445555")
		}
	}
	if len(r.Permissions) > 0 {
		permissions := []string{"employee", "company", "customer"}
		for _, p := range r.Permissions {
			if !adioo.StringInSlice(p, permissions) {
				return fmt.Errorf("invalid permission %s", p)
			}
		}
	}
	if r.AvatarURL != "" {
		_, urlErr := url.ParseRequestURI(r.AvatarURL)
		if urlErr != nil {
			return fmt.Errorf("invalid avatar url %s", r.AvatarURL)
		}
	}
	return nil
}

type updateEmployeeRequest struct {
	ID          string   `json:"id"`
	FirstName   string   `json:"first_name"`
	LastName    string   `json:"last_name"`
	Email       string   `json:"email"`
	PhoneNumber string   `json:"phone_number"`
	Permissions []string `json:"permissions"`
	AvatarURL   string   `json:"avatar_url"`
}

func (r *updateEmployeeRequest) Validate() error {
	switch {
	case !govalidator.IsUUIDv4(r.ID):
		return errors.New("id is invalid")
	case r.FirstName == "":
		return errors.New("first_name is required")
	case r.LastName == "":
		return errors.New("last_name is required")
	case r.Email == "":
		return errors.New("email is required")
	case !govalidator.IsEmail(r.Email):
		return errors.New("invalid email")
	}
	if r.PhoneNumber != "" {
		if len(r.PhoneNumber) != 10 {
			return errors.New("phone number is invalid. Only provide numeric characters. Ex. 9544445555")
		}
	}
	if len(r.Permissions) > 0 {
		permissions := []string{"employee", "company", "customer"}
		for _, p := range r.Permissions {
			if !adioo.StringInSlice(p, permissions) {
				return fmt.Errorf("invalid permission %s", p)
			}
		}
	}
	if r.AvatarURL != "" {
		_, urlErr := url.ParseRequestURI(r.AvatarURL)
		if urlErr != nil {
			return fmt.Errorf("invalid avatar url %s", r.AvatarURL)
		}
	}
	return nil
}
