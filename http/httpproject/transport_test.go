package httpproject

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badProjectRequest struct {
	Name int `json:"name"`
}

func TestHandleCreateProject(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&createProjectRequest{Name: "myname", CompanyID: mock.TestID, CustomerID: mock.TestID, CategoryID: mock.TestID}, http.StatusCreated},
		{&createProjectRequest{}, http.StatusBadRequest},
		{&badProjectRequest{Name: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := "/projects"

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/projects", middleware.GenerateRequestID(testHandler.HandleCreateProject())).Methods("POST")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandleGetProject(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invalid", http.StatusBadRequest},
		{mock.WrongTestID, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/projects/%s", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/projects/{id}", middleware.GenerateRequestID(testHandler.HandleGetProject())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandleUpdateProject(t *testing.T) {
	tests := []struct {
		payload  interface{}
		id       string
		respCode int
	}{
		{&updateProjectRequest{Name: "myname", CustomerID: mock.TestID, CategoryID: mock.TestID}, mock.TestID, http.StatusOK},
		{&updateProjectRequest{}, mock.TestID, http.StatusBadRequest},
		{&badProjectRequest{Name: 1}, mock.TestID, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/projects/%s", test.id)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/projects/{id}", middleware.GenerateRequestID(testHandler.HandleUpdateProject())).Methods("PUT")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandleListCompanyProjects(t *testing.T) {
	tests := []struct {
		id       string
		category string
		respCode int
	}{
		{mock.TestID, "", http.StatusOK},
		{mock.TestID, "mycategory", http.StatusOK},
		{"invalid", "", http.StatusBadRequest},
		{mock.WrongTestID, "", http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/companies/%s/projects", test.id)
		if test.category != "" {
			url += fmt.Sprintf("?category=%s", test.category)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies/{company_id}/projects", middleware.GenerateRequestID(testHandler.HandleListCompanyProjects())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

type badProjectCategoryRequest struct {
	Name int `json:"name"`
}

func TestHandleCreateProjectCategory(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&createProjectCategoryRequest{CompanyID: mock.TestID, Name: "myname"}, http.StatusCreated},
		{&createProjectCategoryRequest{}, http.StatusBadRequest},
		{&badProjectCategoryRequest{Name: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := "/categories"

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/categories", middleware.GenerateRequestID(testHandler.HandleCreateProjectCategory())).Methods("POST")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandleListCompanyCategories(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invalid", http.StatusBadRequest},
		{mock.WrongTestID, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/companies/%s/categories", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies/{company_id}/categories", middleware.GenerateRequestID(testHandler.HandleListCompanyCategories())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}
