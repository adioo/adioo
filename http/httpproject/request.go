package httpproject

import (
	"errors"
	"fmt"
	"net/url"

	"github.com/asaskevich/govalidator"
)

type createProjectRequest struct {
	CompanyID    string `json:"company_id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	CustomerID   string `json:"customer_id"`
	Location     string `json:"location"`
	CategoryID   string `json:"category_id"`
	ThumbnailURL string `json:"thumbnail_url"`
}

func (r *createProjectRequest) Validate() error {
	switch {
	case r.CompanyID == "":
		return errors.New("company_id is required")
	case !govalidator.IsUUIDv4(r.CompanyID):
		return errors.New("invalid company id")
	case r.CustomerID == "":
		return errors.New("customer id is required")
	case !govalidator.IsUUIDv4(r.CustomerID):
		return errors.New("invalid customer id")
	case r.Name == "":
		return errors.New("name is required")
	case r.CategoryID == "":
		return errors.New("category_id is required")
	case !govalidator.IsUUIDv4(r.CategoryID):
		return errors.New("invalid category id")
	}
	if r.ThumbnailURL != "" {
		_, err := url.ParseRequestURI(r.ThumbnailURL)
		if err != nil {
			return fmt.Errorf("invalid url %s", r.ThumbnailURL)
		}
	}
	return nil
}

type updateProjectRequest struct {
	ID           string `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	CustomerID   string `json:"customer_id"`
	Location     string `json:"location"`
	CategoryID   string `json:"category_id"`
	ThumbnailURL string `json:"thumbnail_url"`
}

func (r *updateProjectRequest) Validate() error {
	switch {
	case !govalidator.IsUUIDv4(r.ID):
		return errors.New("invalid project id")
	case r.CustomerID == "":
		return errors.New("customer id is required")
	case !govalidator.IsUUIDv4(r.CustomerID):
		return errors.New("invalid customer id")
	case r.Name == "":
		return errors.New("name is required")
	case r.CategoryID == "":
		return errors.New("category_id is required")
	case !govalidator.IsUUIDv4(r.CategoryID):
		return errors.New("invalid category id")
	}
	if r.ThumbnailURL != "" {
		_, err := url.ParseRequestURI(r.ThumbnailURL)
		if err != nil {
			return fmt.Errorf("invalid url %s", r.ThumbnailURL)
		}
	}
	return nil
}

type createProjectCategoryRequest struct {
	CompanyID   string `json:"company_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (r *createProjectCategoryRequest) Validate() error {
	switch {
	case r.CompanyID == "":
		return errors.New("company_id is required")
	case !govalidator.IsUUIDv4(r.CompanyID):
		return errors.New("invalid company id")
	case r.Name == "":
		return errors.New("name is required")
	}
	return nil
}
