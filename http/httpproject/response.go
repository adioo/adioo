package httpproject

import (
	"fmt"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type sanitizedProject struct {
	ID           string                      `json:"id"`
	Name         string                      `json:"name"`
	Description  string                      `json:"description"`
	Location     string                      `json:"location"`
	ThumbnailURL string                      `json:"thumbnail_url"`
	Category     string                      `json:"category"`
	Company      *response.RelationshipData  `json:"company"`
	Customer     *response.RelationshipData  `json:"customer"`
	Folders      *response.RelationshipsData `json:"folders"`
	Photos       *response.RelationshipsData `json:"photos"`
	CreatedAt    time.Time                   `json:"created_at"`
	UpdatedAt    time.Time                   `json:"updated_at"`
}

type sanitizedProjectFolder struct {
	ID string `json:"id"`
}

type projectResponse struct {
	Data *sanitizedProject `json:"data"`
}

type projectsResponse struct {
	response.PaginationData
	Data []*sanitizedProject `json:"data"`
}

type projectCategoryResponse struct {
	Data *adioo.ProjectCategory `json:"data"`
}

type projectCategoriesResponse struct {
	response.PaginationData
	Data []*adioo.ProjectCategory `json:"data"`
}

func sanitizeProject(p *adioo.Project, projectSvc adioo.ProjectService) *sanitizedProject {
	var category *adioo.ProjectCategory
	c, err := projectSvc.Category(p.CategoryID)
	if err == nil {
		category = c
	}

	return &sanitizedProject{
		ID: p.ID,
		Company: &response.RelationshipData{
			ID:   p.CompanyID,
			HREF: fmt.Sprintf("/companies/%s", p.CompanyID),
		},
		Category: category.Name,
		Name:     p.Name,
		Customer: &response.RelationshipData{
			ID:   p.CustomerID,
			HREF: fmt.Sprintf("/customers/%s", p.CustomerID),
		},
		Folders: &response.RelationshipsData{
			HREF: fmt.Sprintf("/projects/%s/folders", p.ID),
		},
		Photos: &response.RelationshipsData{
			HREF: fmt.Sprintf("/projects/%s/photos", p.ID),
		},
		Description:  p.Description,
		Location:     p.Location,
		ThumbnailURL: p.ThumbnailURL,
		CreatedAt:    p.CreatedAt,
		UpdatedAt:    p.UpdatedAt,
	}
}

func sanitizeProjects(s []*adioo.Project, projectSvc adioo.ProjectService) []*sanitizedProject {
	ss := make([]*sanitizedProject, len(s))
	for x, u := range s {
		nu := sanitizeProject(u, projectSvc)
		ss[x] = nu
	}

	return ss
}
