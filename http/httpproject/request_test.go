package httpproject

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"
)

func TestCreateProjectRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *createProjectRequest
		expected bool
	}{
		{&createProjectRequest{Name: "myname", CompanyID: mock.TestID, CustomerID: mock.TestID, CategoryID: mock.TestID}, true},
		{&createProjectRequest{Name: "myname", CompanyID: mock.TestID, CustomerID: mock.TestID, CategoryID: mock.TestID, ThumbnailURL: "invalid"}, false},
		{&createProjectRequest{Name: "myname", CompanyID: mock.TestID, CustomerID: mock.TestID, CategoryID: "invalid"}, false},
		{&createProjectRequest{Name: "myname", CompanyID: mock.TestID, CustomerID: "invalid", CategoryID: mock.TestID}, false},
		{&createProjectRequest{Name: "myname", CompanyID: "invalid", CustomerID: mock.TestID, CategoryID: mock.TestID}, false},
		{&createProjectRequest{Name: "myname", CompanyID: mock.TestID, CustomerID: mock.TestID}, false},
		{&createProjectRequest{Name: "myname", CompanyID: mock.TestID, CategoryID: mock.TestID}, false},
		{&createProjectRequest{Name: "myname", CustomerID: mock.TestID, CategoryID: mock.TestID}, false},
		{&createProjectRequest{CompanyID: mock.TestID, CustomerID: mock.TestID, CategoryID: mock.TestID}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}

func TestUpdateProjectRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *updateProjectRequest
		expected bool
	}{
		{&updateProjectRequest{Name: "myname", ID: mock.TestID, CustomerID: mock.TestID, CategoryID: mock.TestID}, true},
		{&updateProjectRequest{Name: "myname", ID: mock.TestID, CustomerID: mock.TestID, CategoryID: mock.TestID, ThumbnailURL: "invalid"}, false},
		{&updateProjectRequest{Name: "myname", ID: mock.TestID, CustomerID: mock.TestID, CategoryID: "invalid"}, false},
		{&updateProjectRequest{Name: "myname", ID: mock.TestID, CustomerID: "invalid", CategoryID: mock.TestID}, false},
		{&updateProjectRequest{Name: "myname", ID: "invalid", CustomerID: mock.TestID, CategoryID: mock.TestID}, false},
		{&updateProjectRequest{Name: "myname", ID: mock.TestID, CustomerID: mock.TestID}, false},
		{&updateProjectRequest{Name: "myname", ID: mock.TestID, CategoryID: mock.TestID}, false},
		{&updateProjectRequest{Name: "myname", CustomerID: mock.TestID, CategoryID: mock.TestID}, false},
		{&updateProjectRequest{ID: mock.TestID, CustomerID: mock.TestID, CategoryID: mock.TestID}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}

func TestCreateProjectCategoryRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *createProjectCategoryRequest
		expected bool
	}{
		{&createProjectCategoryRequest{CompanyID: mock.TestID, Name: "myname"}, true},
		{&createProjectCategoryRequest{CompanyID: "invalid", Name: "myname"}, false},
		{&createProjectCategoryRequest{CompanyID: mock.TestID}, false},
		{&createProjectCategoryRequest{Name: "myname"}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}
