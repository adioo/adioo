package httpproject

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	ProjectStorage adioo.ProjectService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(s adioo.ProjectService) *Handler {
	return &Handler{
		Router:         mux.NewRouter(),
		ProjectStorage: s,
	}
}
