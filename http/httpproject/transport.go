package httpproject

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreateProject is
func (h *Handler) HandleCreateProject() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createProjectRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewProject{
			Name:        payload.Name,
			CustomerID:  payload.CustomerID,
			Description: payload.Description,
			CategoryID:  payload.CategoryID,
			Location:    payload.Location,
			CompanyID:   payload.CompanyID,
		}

		p, err := h.ProjectStorage.Create(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := projectResponse{
			Data: sanitizeProject(p, h.ProjectStorage),
		}

		response.Created(w, resp)
	})
}

// HandleGetProject is
func (h *Handler) HandleGetProject() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]
		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		p, err := h.ProjectStorage.Project(id)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}
		resp := projectResponse{
			Data: sanitizeProject(p, h.ProjectStorage),
		}
		response.OK(w, resp)
	})
}

// HandleUpdateProject is
func (h *Handler) HandleUpdateProject() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(updateProjectRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		id := mux.Vars(r)["id"]
		payload.ID = id

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		u := &adioo.UpdateProject{
			ID:          payload.ID,
			Name:        payload.Name,
			CustomerID:  payload.CustomerID,
			Description: payload.Description,
			CategoryID:  payload.CategoryID,
			Location:    payload.Location,
		}

		p, err := h.ProjectStorage.Update(u)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := projectResponse{
			Data: sanitizeProject(p, h.ProjectStorage),
		}

		response.OK(w, resp)
	})
}

// HandleListCompanyProjects is
func (h *Handler) HandleListCompanyProjects() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		companyID := mux.Vars(r)["company_id"]
		category := "all"

		if !govalidator.IsUUIDv4(companyID) {
			response.WithError(w, errors.New("invalid company id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		q := r.URL.Query()
		if q.Get("category") != "" {
			categoryName := q.Get("category")
			c, err := h.ProjectStorage.CompanyCategory(companyID, categoryName)
			if err != nil {
				response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
				return
			}
			category = c.ID
		}

		p, err := h.ProjectStorage.ListCompanyProjects(companyID, category)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}
		resp := projectsResponse{
			Data: sanitizeProjects(p, h.ProjectStorage),
		}
		response.OK(w, resp)
	})
}

// HandleCreateProjectCategory is
func (h *Handler) HandleCreateProjectCategory() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createProjectCategoryRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		c, err := h.ProjectStorage.CreateCategory(payload.CompanyID, payload.Name, payload.Description)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := projectCategoryResponse{
			Data: c,
		}

		response.Created(w, resp)
	})
}

// HandleListCompanyCategories is
func (h *Handler) HandleListCompanyCategories() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		companyID := mux.Vars(r)["company_id"]

		if !govalidator.IsUUIDv4(companyID) {
			response.WithError(w, errors.New("invalid company id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		c, err := h.ProjectStorage.ListCompanyCategories(companyID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := projectCategoriesResponse{
			Data: c,
		}
		response.OK(w, resp)
	})
}
