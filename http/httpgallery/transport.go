package httpgallery

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreateGallery is
func (h *Handler) HandleCreateGallery() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createGalleryRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		decryptedURLs, err := h.EncryptionService.DecryptURLs(payload.URLs)
		if err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		g, err := h.GalleryStorage.Create(payload.Title, payload.EmployeeID, decryptedURLs, payload.Permissions)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &galleryResponse{
			Data: sanitizeGallery(g),
		}

		response.Created(w, resp)
	})
}

// HandleGetGallery is
func (h *Handler) HandleGetGallery() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		g, err := h.GalleryStorage.Gallery(id)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &galleryResponse{
			Data: sanitizeGallery(g),
		}

		response.OK(w, resp)
	})
}

// HandleAddGalleryPhoto is
func (h *Handler) HandleAddGalleryPhoto() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(addGalleryPhoto)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}
		if payload.URL == "" {
			response.WithError(w, errors.New("url is required"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}
		galleryID := mux.Vars(r)["gallery_id"]

		if !govalidator.IsUUIDv4(galleryID) {
			response.WithError(w, errors.New("invalid gallery id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		p, err := h.GalleryStorage.AddPhoto(galleryID, payload.URL)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		sp, err := sanitizeGalleryPhoto(p, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &galleryPhotoResponse{
			Data: sp,
		}

		response.Created(w, resp)
	})
}

// HandleRemoveGalleryPhoto is
func (h *Handler) HandleRemoveGalleryPhoto() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		photoID := mux.Vars(r)["photo_id"]
		galleryID := mux.Vars(r)["gallery_id"]

		if !govalidator.IsUUIDv4(photoID) {
			response.WithError(w, errors.New("invalid photo id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if !govalidator.IsUUIDv4(galleryID) {
			response.WithError(w, errors.New("invalid gallery id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := h.GalleryStorage.RemovePhoto(photoID); err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &response.Message{
			Data: &response.MessageData{Message: "photo was removed"},
		}

		response.OK(w, resp)
	})
}

// HandleListGalleryPhotos is
func (h *Handler) HandleListGalleryPhotos() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		g, err := h.GalleryStorage.Gallery(id)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		var permissions []string
		for _, per := range g.Permissions {
			permissions = append(permissions, per.Name)
		}

		sp, err := sanitizeGalleryPhotos(g.Photos, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &galleryPhotosResponse{
			Data: &galleryPhotoRespData{
				Gallery: &response.RelationshipData{
					ID:   id,
					HREF: fmt.Sprintf("/galleries/%s", id)},
				Photos: sp,
			},
		}

		response.OK(w, resp)

	})
}

// HandleListGalleryPhotoComments is
func (h *Handler) HandleListGalleryPhotoComments() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		photoID := mux.Vars(r)["photo_id"]

		if !govalidator.IsUUIDv4(photoID) {
			response.WithError(w, errors.New("invalid photo id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		c, err := h.GalleryStorage.ListPhotoComments(photoID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		if c != nil {
			p, err := h.GalleryStorage.GetPhoto(photoID)
			if err != nil {
				response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
				return
			}
			resp := &galleryPhotoCommentsResponse{
				Data: sanitizeGalleryPhotoComments(c, p.GalleryID),
			}

			response.OK(w, resp)
			return
		}

		resp := &galleryPhotoCommentsResponse{
			Data: []*sanitizedGalleryPhotoComment{},
		}

		response.OK(w, resp)

	})
}

// HandleGetGalleryPhoto is
func (h *Handler) HandleGetGalleryPhoto() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		photoID := mux.Vars(r)["photo_id"]
		galleryID := mux.Vars(r)["gallery_id"]

		if !govalidator.IsUUIDv4(photoID) {
			response.WithError(w, errors.New("invalid photo id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if !govalidator.IsUUIDv4(galleryID) {
			response.WithError(w, errors.New("invalid gallery id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		p, err := h.GalleryStorage.GetPhoto(photoID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		sp, err := sanitizeGalleryPhoto(p, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &galleryPhotoResponse{
			Data: sp,
		}

		response.OK(w, resp)

	})
}

// HandleListEmployeeGalleries is
func (h *Handler) HandleListEmployeeGalleries() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		employeeID := mux.Vars(r)["employee_id"]

		if !govalidator.IsUUIDv4(employeeID) {
			response.WithError(w, errors.New("invalid employee id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		g, err := h.GalleryStorage.ListEmployeeGalleries(employeeID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		if g == nil {
			g = []*adioo.Gallery{}
		}

		resp := &galleriesResponse{
			Data: sanitizeGalleries(g),
		}

		response.OK(w, resp)

	})
}

// HandleListCompanyGalleries is
func (h *Handler) HandleListCompanyGalleries() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		companyID := mux.Vars(r)["company_id"]

		if !govalidator.IsUUIDv4(companyID) {
			response.WithError(w, errors.New("invalid company id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		g, err := h.GalleryStorage.ListCompanyGalleries(companyID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		if g == nil {
			g = []*adioo.Gallery{}
		}

		resp := &galleriesResponse{
			Data: sanitizeGalleries(g),
		}

		response.OK(w, resp)

	})
}

// HandleCreateGalleryPhotoComment is
func (h *Handler) HandleCreateGalleryPhotoComment() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createPhotoCommentRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		galleryID := mux.Vars(r)["gallery_id"]
		photoID := mux.Vars(r)["photo_id"]

		if !govalidator.IsUUIDv4(galleryID) {
			response.WithError(w, errors.New("gallery id invalid"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if !govalidator.IsUUIDv4(photoID) {
			response.WithError(w, errors.New("photo id invalid"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewGalleryPhotoComment{
			PhotoID:    payload.PhotoID,
			Text:       payload.Text,
			Anchor:     payload.Anchor,
			CustomerID: payload.CustomerID,
		}

		c, err := h.GalleryStorage.CreatePhotoComment(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &galleryPhotoCommentResponse{
			Data: &sanitizedGalleryPhotoComment{
				ID:     c.ID,
				Text:   c.Text,
				Anchor: c.Anchor,
				Photo: &response.RelationshipData{
					ID:   c.PhotoID,
					HREF: fmt.Sprintf("/galleries/%s/photos/%s", galleryID, c.PhotoID)},
				CustomerID: nil,
				CreatedAt:  c.CreatedAt,
				UpdatedAt:  c.UpdatedAt,
			},
		}

		response.Created(w, resp)

	})
}
