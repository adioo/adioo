package httpgallery

import (
	"errors"
	"fmt"

	"github.com/asaskevich/govalidator"
	"gitlab.com/adioo/adioo"
)

type createGalleryRequest struct {
	Title       string   `json:"title"`
	EmployeeID  string   `json:"employee_id"`
	URLs        []string `json:"urls"`
	Permissions []string `json:"permissions"`
}

func (r *createGalleryRequest) Validate() error {
	switch {
	case r.Title == "":
		return errors.New("title is required")
	case r.EmployeeID == "":
		return errors.New("employee_id is required")
	case !govalidator.IsUUIDv4(r.EmployeeID):
		return errors.New("invalid employee_id")
	case len(r.URLs) == 0:
		return errors.New("at least 1 photo is required")
	}

	// if we reach this loop we know at least 1 photo id was passed in
	for i, photo := range r.URLs {
		if !govalidator.IsURL(photo) {
			return fmt.Errorf("invalid photo url %s. This is just a guess but its probably index %d in the array", photo, i)
		}
	}

	if len(r.Permissions) > 0 {
		allowedPermissions := []string{"downloadable"}
		for _, p := range r.Permissions {
			if !adioo.StringInSlice(p, allowedPermissions) {
				return fmt.Errorf("invalid permission %s", p)
			}
		}
	}

	return nil
}

type addGalleryPhoto struct {
	URL string `json:"url"`
}

type createPhotoCommentRequest struct {
	PhotoID    string            `json:"photo_id"`
	Text       string            `json:"text"`
	Anchor     adioo.PhotoAnchor `json:"anchor"`
	CustomerID string            `json:"customer_id"`
}

func (r *createPhotoCommentRequest) Validate() error {
	switch {
	case r.PhotoID == "":
		return errors.New("photo id is required")
	case !govalidator.IsUUIDv4(r.PhotoID):
		return errors.New("photo id is invalid")
	case r.Text == "":
		return errors.New("text is required")
	}

	if r.CustomerID != "" && !govalidator.IsUUIDv4(r.CustomerID) {
		return errors.New("invalid customer id")
	}

	return nil
}
