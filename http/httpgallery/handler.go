package httpgallery

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	GalleryStorage    adioo.GalleryService
	EncryptionService adioo.EncryptionService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(s adioo.GalleryService, e adioo.EncryptionService) *Handler {
	return &Handler{
		Router:            mux.NewRouter(),
		GalleryStorage:    s,
		EncryptionService: e,
	}
}
