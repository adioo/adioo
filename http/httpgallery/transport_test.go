package httpgallery

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badGalleryRequest struct {
	Title int `json:"title"`
}

func TestHandleCreateGallery(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&createGalleryRequest{Title: "testtitle", EmployeeID: uuid.NewV4().String(), URLs: []string{testURL}}, http.StatusCreated},
		{&createGalleryRequest{EmployeeID: uuid.NewV4().String(), URLs: []string{testURL}}, http.StatusBadRequest},
		{&badGalleryRequest{Title: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := "/galleries"

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries", middleware.GenerateRequestID(testHandler.HandleCreateGallery())).Methods("POST")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandleGetGallery(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invaliduuid", http.StatusBadRequest},
		{mock.WrongTestID, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/galleries/%s", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries/{id}", middleware.GenerateRequestID(testHandler.HandleGetGallery())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}
}

type testBadGalleryPhoto struct {
	URL int
}

func TestHandleAddGalleryPhoto(t *testing.T) {
	tests := []struct {
		payload  interface{}
		id       string
		respCode int
	}{
		{&addGalleryPhoto{URL: testURL}, mock.TestID, http.StatusCreated},
		{&addGalleryPhoto{}, mock.TestID, http.StatusBadRequest},
		{&addGalleryPhoto{URL: testURL}, "invaliduuid", http.StatusBadRequest},
		{&testBadGalleryPhoto{URL: 1}, "invaliduuid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/galleries/%s/photos", test.id)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries/{gallery_id}/photos", middleware.GenerateRequestID(testHandler.HandleAddGalleryPhoto())).Methods("POST")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

func TestHandleRemoveGalleryPhoto(t *testing.T) {
	tests := []struct {
		galleryID string
		PhotoID   string
		respCode  int
	}{
		{mock.TestID, mock.TestID, http.StatusOK},
		{mock.TestID, mock.WrongTestID, http.StatusNotFound},
		{mock.TestID, "invaliduuid", http.StatusBadRequest},
		{"invaliduuid", mock.TestID, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/galleries/%s/photos/%s", test.galleryID, test.PhotoID)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries/{gallery_id}/photos/{photo_id}", middleware.GenerateRequestID(testHandler.HandleRemoveGalleryPhoto())).Methods("DELETE")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

func TestHandleListGalleryPhotos(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{mock.WrongTestID, http.StatusNotFound},
		{"invaliduuid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/galleries/%s/photos", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries/{id}/photos", middleware.GenerateRequestID(testHandler.HandleListGalleryPhotos())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

func TestHandleListGalleryPhotoComments(t *testing.T) {
	tests := []struct {
		galleryID string
		photoID   string
		respCode  int
	}{
		{mock.TestID, mock.TestID, http.StatusOK},
		{mock.TestID, mock.WrongTestID, http.StatusNotFound},
		{mock.TestID, "invalid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/galleries/%s/photos/%s/comments", test.galleryID, test.photoID)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries/{gallery_id}/photos/{photo_id}/comments", middleware.GenerateRequestID(testHandler.HandleListGalleryPhotoComments())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

func TestHandleGetGalleryPhoto(t *testing.T) {
	tests := []struct {
		galleryID string
		photoID   string
		respCode  int
	}{
		{mock.TestID, mock.TestID, http.StatusOK},
		{mock.TestID, mock.WrongTestID, http.StatusNotFound},
		{mock.TestID, "invalid", http.StatusBadRequest},
		{"invalid", mock.TestID, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/galleries/%s/photos/%s", test.galleryID, test.photoID)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries/{gallery_id}/photos/{photo_id}", middleware.GenerateRequestID(testHandler.HandleGetGalleryPhoto())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

func TestHandleListEmployeeGalleries(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{mock.WrongTestID, http.StatusNotFound},
		{"invaliduuid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/employees/%s/galleries", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/employees/{employee_id}/galleries", middleware.GenerateRequestID(testHandler.HandleListEmployeeGalleries())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

func TestHandleListCompanyGalleries(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{mock.WrongTestID, http.StatusNotFound},
		{"invaliduuid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/companies/%s/galleries", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies/{company_id}/galleries", middleware.GenerateRequestID(testHandler.HandleListCompanyGalleries())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

type testBadGalleryPhotoComment struct {
	Text int
}

func TestHandleCreateGalleryPhotoComment(t *testing.T) {
	tests := []struct {
		payload   interface{}
		galleryID string
		photoID   string
		respCode  int
	}{
		{&createPhotoCommentRequest{PhotoID: mock.TestID, Text: "mycomment"}, mock.TestID, mock.TestID, http.StatusCreated},
		{&createPhotoCommentRequest{PhotoID: mock.TestID, Text: "mycomment"}, mock.TestID, mock.TestID, http.StatusCreated},
		{&createPhotoCommentRequest{PhotoID: mock.TestID, Text: "mycomment"}, mock.TestID, "invalidid", http.StatusBadRequest},
		{&createPhotoCommentRequest{PhotoID: mock.TestID, Text: "mycomment"}, "invalid id", mock.TestID, http.StatusBadRequest},
		{&createPhotoCommentRequest{Text: "mycomment"}, mock.TestID, mock.TestID, http.StatusBadRequest},
		{&testBadGalleryPhotoComment{Text: 1}, mock.TestID, mock.TestID, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/galleries/%s/photos/%s/comments", test.galleryID, test.photoID)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/galleries/{gallery_id}/photos/{photo_id}/comments", middleware.GenerateRequestID(testHandler.HandleCreateGalleryPhotoComment())).Methods("POST")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}
