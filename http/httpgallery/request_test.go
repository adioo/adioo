package httpgallery

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"

	uuid "github.com/satori/go.uuid"
)

const testURL = "http://myurl.com"

func TestCreateGalleryRequest_Validate(t *testing.T) {
	tt := []struct {
		req      *createGalleryRequest
		expected bool
	}{
		{&createGalleryRequest{Title: "testtitle", EmployeeID: uuid.NewV4().String(), URLs: []string{testURL}}, true},
		{&createGalleryRequest{Title: "testtitle", EmployeeID: uuid.NewV4().String(), URLs: []string{testURL}, Permissions: []string{"downloadable"}}, true},
		{&createGalleryRequest{Title: "testtitle", EmployeeID: uuid.NewV4().String(), URLs: []string{testURL}, Permissions: []string{"wrongpermision"}}, false},
		{&createGalleryRequest{Title: "testtitle", EmployeeID: uuid.NewV4().String(), URLs: []string{}}, false},
		{&createGalleryRequest{Title: "testtitle", URLs: []string{testURL}}, false},
		{&createGalleryRequest{EmployeeID: uuid.NewV4().String(), URLs: []string{testURL}}, false},
		{&createGalleryRequest{Title: "testtitle", EmployeeID: uuid.NewV4().String(), URLs: []string{testURL, "wrongurl"}}, false},
		{&createGalleryRequest{Title: "testtitle", EmployeeID: "wrong id", URLs: []string{testURL}}, false},
	}

	for _, test := range tt {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}

func TestCreatePhotoCommentRequest_Validate(t *testing.T) {
	tt := []struct {
		req      *createPhotoCommentRequest
		expected bool
	}{
		{&createPhotoCommentRequest{PhotoID: mock.TestID, Text: "mycomment"}, true},
		{&createPhotoCommentRequest{PhotoID: mock.TestID, Text: "mycomment", CustomerID: "invalidid"}, false},
		{&createPhotoCommentRequest{PhotoID: "invaliduuid", Text: "mycomment"}, false},
		{&createPhotoCommentRequest{PhotoID: mock.TestID}, false},
		{&createPhotoCommentRequest{Text: "mycomment"}, false},
	}

	for _, test := range tt {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}
