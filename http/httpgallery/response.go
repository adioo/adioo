package httpgallery

import (
	"fmt"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type galleryResponse struct {
	Data *sanitizedGallery `json:"data"`
}

type galleriesResponse struct {
	Data []*sanitizedGallery `json:"data"`
}

type sanitizedGallery struct {
	ID          string                      `json:"id"`
	Title       string                      `json:"title"`
	Permissions []string                    `json:"permissions"`
	Employee    *response.RelationshipData  `json:"employee"`
	Photos      *response.RelationshipsData `json:"photos"`
	CreatedAt   time.Time                   `json:"created_at"`
	UpdatedAt   time.Time                   `json:"updated_at"`
}

func sanitizeGallery(g *adioo.Gallery) *sanitizedGallery {
	permissions := []string{}
	for _, p := range g.Permissions {
		permissions = append(permissions, p.Name)
	}

	return &sanitizedGallery{
		ID:          g.ID,
		Title:       g.Title,
		Permissions: permissions,
		Employee: &response.RelationshipData{
			ID:   g.EmployeeID,
			HREF: fmt.Sprintf("/employees/%s", g.EmployeeID),
		},
		Photos: &response.RelationshipsData{
			HREF: fmt.Sprintf("/galleries/%s/photos", g.ID),
		},
		CreatedAt: g.CreatedAt,
		UpdatedAt: g.UpdatedAt,
	}
}

func sanitizeGalleries(s []*adioo.Gallery) []*sanitizedGallery {
	ss := make([]*sanitizedGallery, len(s))
	for x, u := range s {
		nu := sanitizeGallery(u)
		ss[x] = nu
	}

	return ss
}

type galleryPhotoResponse struct {
	Data *sanitizedGalleryPhoto `json:"data"`
}

type galleryPhotosResponse struct {
	Data *galleryPhotoRespData `json:"data"`
}

type galleryPhotoRespData struct {
	Gallery *response.RelationshipData `json:"gallery"`
	Photos  []*sanitizedGalleryPhoto   `json:"photos"`
}

type sanitizedGalleryPhoto struct {
	ID              string                      `json:"id"`
	Src             string                      `json:"url"`
	IsOriginal      bool                        `json:"is_original"`
	GalleryPhotoID  string                      `json:"parent_id"`
	Transformations []*sanitizedGalleryPhoto    `json:"transformations"`
	Gallery         *response.RelationshipData  `json:"gallery"`
	Comments        *response.RelationshipsData `json:"comments"`
}

func sanitizeGalleryPhoto(p *adioo.GalleryPhoto, e adioo.EncryptionService) (*sanitizedGalleryPhoto, error) {
	fullURL, err := e.EncryptURL(p.Src)
	if err != nil {
		return nil, err
	}
	return &sanitizedGalleryPhoto{
		ID:             p.ID,
		Src:            fullURL,
		IsOriginal:     p.IsOriginal,
		GalleryPhotoID: p.GalleryPhotoID,
		Gallery: &response.RelationshipData{
			ID:   p.GalleryID,
			HREF: fmt.Sprintf("/galleries/%s", p.GalleryID),
		},
		Comments: &response.RelationshipsData{
			HREF: fmt.Sprintf("/galleries/%s/photos/%s/comments", p.GalleryID, p.ID),
		},
	}, nil
}

func sanitizeGalleryPhotos(s []*adioo.GalleryPhoto, e adioo.EncryptionService) ([]*sanitizedGalleryPhoto, error) {
	ss := make([]*sanitizedGalleryPhoto, len(s))
	for x, u := range s {
		nu, err := sanitizeGalleryPhoto(u, e)
		if err != nil {
			return nil, err
		}
		ss[x] = nu
	}

	return ss, nil
}

type galleryPhotoCommentResponse struct {
	Data *sanitizedGalleryPhotoComment `json:"data"`
}

type galleryPhotoCommentsResponse struct {
	Data []*sanitizedGalleryPhotoComment `json:"data"`
}

type sanitizedGalleryPhotoComment struct {
	ID         string                     `json:"id"`
	Text       string                     `json:"text"`
	Anchor     adioo.PhotoAnchor          `json:"anchor"`
	Photo      *response.RelationshipData `json:"photo"`
	CustomerID *response.RelationshipData `json:"customer"`
	CreatedAt  time.Time                  `json:"created_at"`
	UpdatedAt  time.Time                  `json:"updated_at"`
}

func sanitizeGalleryPhotoComment(p *adioo.GalleryPhotoComment, galleryID string) *sanitizedGalleryPhotoComment {
	return &sanitizedGalleryPhotoComment{
		ID:     p.ID,
		Text:   p.Text,
		Anchor: p.Anchor,
		Photo: &response.RelationshipData{
			ID:   p.PhotoID,
			HREF: fmt.Sprintf("/galleries/%s/photos/%s", galleryID, p.PhotoID),
		},
		CustomerID: nil,
		CreatedAt:  p.CreatedAt,
		UpdatedAt:  p.UpdatedAt,
	}
}

func sanitizeGalleryPhotoComments(s []*adioo.GalleryPhotoComment, galleryID string) []*sanitizedGalleryPhotoComment {
	ss := make([]*sanitizedGalleryPhotoComment, len(s))
	for x, u := range s {
		nu := sanitizeGalleryPhotoComment(u, galleryID)
		ss[x] = nu
	}

	return ss
}
