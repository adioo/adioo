package httpcustomer

import (
	"errors"

	"github.com/asaskevich/govalidator"
)

type createCustomerRequest struct {
	CompanyID   string `json:"company_id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
}

func (r *createCustomerRequest) Validate() error {
	switch {
	case r.CompanyID == "":
		return errors.New("company_id is required")
	case !govalidator.IsUUIDv4(r.CompanyID):
		return errors.New("company id is invalid")
	case r.FirstName == "":
		return errors.New("first_name is required")
	case r.LastName == "":
		return errors.New("last_name is required")
	}
	if r.Email != "" {
		if !govalidator.IsEmail(r.Email) {
			return errors.New("email is invalid")
		}
	}
	if r.PhoneNumber != "" {
		if len(r.PhoneNumber) != 10 {
			return errors.New("phone number is invalid. Only provide numeric characters. Ex. 9544445555")
		}
	}
	return nil
}
