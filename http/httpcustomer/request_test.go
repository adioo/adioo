package httpcustomer

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"
)

func TestCreateCustomerRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *createCustomerRequest
		expected bool
	}{
		{&createCustomerRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, true},
		{&createCustomerRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1"}, false},
		{&createCustomerRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "invalidemail.com", PhoneNumber: "1234560909"}, false},
		{&createCustomerRequest{CompanyID: mock.TestID, FirstName: "fn", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&createCustomerRequest{CompanyID: mock.TestID, LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&createCustomerRequest{CompanyID: "invalidid", FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
		{&createCustomerRequest{FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, result, test.expected)
	}
}
