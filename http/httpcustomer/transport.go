package httpcustomer

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreateCustomer handles the transport of creating a new customer
func (h *Handler) HandleCreateCustomer() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createCustomerRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewCustomer{
			CompanyID:   payload.CompanyID,
			FirstName:   payload.FirstName,
			LastName:    payload.LastName,
			Email:       payload.Email,
			PhoneNumber: payload.PhoneNumber,
		}

		c, err := h.CustomerStorage.Create(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := customerResponse{
			Data: sanitizeCustomer(c),
		}

		response.Created(w, resp)
	})
}

// HandleListCompanyCustomers is
func (h *Handler) HandleListCompanyCustomers() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		companyID := mux.Vars(r)["company_id"]
		if !govalidator.IsUUIDv4(companyID) {
			response.WithError(w, errors.New("invalid company id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		q := r.URL.Query()
		limit := 20
		if q.Get("count") != "" {
			icount, err := strconv.Atoi(q.Get("count"))
			if err != nil {
				response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
				return
			}
			limit = icount
		}
		sinceID := q.Get("since_id")

		c, err := h.CustomerStorage.ListCompanyCustomers(companyID, sinceID, limit)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := customersResponse{
			Data: sanitizeCustomers(c),
		}
		cLen := len(c)
		resp.Count = limit
		if cLen < limit {
			resp.Count = cLen
		}
		if cLen == limit {
			resp.SinceID = c[cLen-1].ID
			resp.Links.Next = fmt.Sprintf("/companies/%s/customers?since_id=%s", companyID, resp.SinceID)
		}
		resp.Links.Self = r.URL.String()

		response.OK(w, resp)
	})
}

// HandleGetCustomer is
func (h *Handler) HandleGetCustomer() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]
		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		c, err := h.CustomerStorage.Customer(id)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := customerResponse{
			Data: sanitizeCustomer(c),
		}

		response.OK(w, resp)
	})
}
