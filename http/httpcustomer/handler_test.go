package httpcustomer

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"
)

func TestNewHandler(t *testing.T) {
	s := NewHandler(&mock.CustomerService{})
	assert.NotNil(t, s)
	assert.NotNil(t, s.Router)
	assert.NotNil(t, s.CustomerStorage)
}
