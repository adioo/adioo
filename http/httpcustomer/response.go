package httpcustomer

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type customerResponse struct {
	Data *sanitizedCustomer `json:"data"`
}

type customersResponse struct {
	response.PaginationData
	Data []*sanitizedCustomer `json:"data"`
}

type sanitizedCustomer struct {
	ID          string                     `json:"id"`
	FirstName   string                     `json:"first_name"`
	LastName    string                     `json:"last_name"`
	Email       string                     `json:"email"`
	PhoneNumber string                     `json:"phone_number"`
	Company     *response.RelationshipData `json:"company"`
	CreatedAt   time.Time                  `json:"created_at"`
	UpdatedAt   time.Time                  `json:"updated_at"`
}

func sanitizeCustomer(p *adioo.Customer) *sanitizedCustomer {
	return &sanitizedCustomer{
		ID: p.ID,
		Company: &response.RelationshipData{
			ID:   p.CompanyID,
			HREF: fmt.Sprintf("/companies/%s", p.CompanyID),
		},
		FirstName:   strings.Title(p.FirstName),
		LastName:    strings.Title(p.LastName),
		Email:       p.Email,
		PhoneNumber: p.PhoneNumber,
		CreatedAt:   p.CreatedAt,
		UpdatedAt:   p.UpdatedAt,
	}
}

func sanitizeCustomers(s []*adioo.Customer) []*sanitizedCustomer {
	ss := make([]*sanitizedCustomer, len(s))
	for x, u := range s {
		nu := sanitizeCustomer(u)
		ss[x] = nu
	}

	return ss
}
