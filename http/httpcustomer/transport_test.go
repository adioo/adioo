package httpcustomer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badCustomerRequest struct {
	FirstName int `json:"first_name"`
}

func TestHandleCreateCustomer(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&createCustomerRequest{CompanyID: mock.TestID, FirstName: mock.TestCustomerName, LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, http.StatusCreated},
		{&createCustomerRequest{CompanyID: mock.TestID}, http.StatusBadRequest},
		{&createCustomerRequest{CompanyID: mock.TestID, FirstName: "fn", LastName: "ln", Email: "test@email.com", PhoneNumber: "1234560909"}, http.StatusInternalServerError},
		{&badCustomerRequest{FirstName: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", "/customers", bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/customers", middleware.GenerateRequestID(testHandler.HandleCreateCustomer()))

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}

func TestHandleListCompanyCustomers(t *testing.T) {
	tests := []struct {
		id       string
		count    string
		respCode int
	}{
		{mock.TestID, "", http.StatusOK},
		{mock.TestID, "20", http.StatusOK},
		{"invalid uuid", "", http.StatusBadRequest},
		{uuid.NewV4().String(), "", http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/companies/%s/customers", test.id)
		if test.count != "" {
			url = fmt.Sprintf("/companies/%s/customers?count=%s", test.id, test.count)
		}
		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies/{company_id}/customers", middleware.GenerateRequestID(testHandler.HandleListCompanyCustomers())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}

func TestGetCustomer(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invaliduuid", http.StatusBadRequest},
		{uuid.NewV4().String(), http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/customers/%s", test.id)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/customers/{id}", middleware.GenerateRequestID(testHandler.HandleGetCustomer())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}
}
