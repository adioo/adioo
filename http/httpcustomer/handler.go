package httpcustomer

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	CustomerStorage adioo.CustomerService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(s adioo.CustomerService) *Handler {
	return &Handler{
		Router:          mux.NewRouter(),
		CustomerStorage: s,
	}
}
