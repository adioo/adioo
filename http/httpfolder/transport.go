package httpfolder

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreateFolder is
func (h *Handler) HandleCreateFolder() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createFolderRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}
		payload.ProjectID = mux.Vars(r)["project_id"]

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewFolder{
			Name:        payload.Name,
			ProjectID:   payload.ProjectID,
			Description: payload.Description,
		}

		f, err := h.FolderStorage.Create(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		sf, err := sanitizeFolder(f, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := folderResponse{
			Data: sf,
		}

		response.Created(w, resp)
	})
}

// HandleListProjectFolders is
func (h *Handler) HandleListProjectFolders() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		projectID := mux.Vars(r)["project_id"]

		f, err := h.FolderStorage.ListProjectFolders(projectID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		sf, err := sanitizeFolders(f, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := foldersResponse{
			Data: sf,
		}
		response.OK(w, resp)
	})
}

// HandleGetFolder is
func (h *Handler) HandleGetFolder() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid folder id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		f, err := h.FolderStorage.Folder(id)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		sf, err := sanitizeFolder(f, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &folderResponse{
			Data: sf,
		}

		response.OK(w, resp)
	})
}
