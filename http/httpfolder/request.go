package httpfolder

import (
	"errors"

	"github.com/asaskevich/govalidator"
)

type createFolderRequest struct {
	ProjectID   string `json:"project_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (r *createFolderRequest) Validate() error {
	switch {
	case r.Name == "":
		return errors.New("name is required")
	case r.ProjectID == "":
		return errors.New("project id is required")
	case !govalidator.IsUUIDv4(r.ProjectID):
		return errors.New("invalid project id")
	}
	return nil
}
