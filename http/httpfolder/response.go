package httpfolder

import (
	"fmt"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type sanitizedFolder struct {
	ID          string                     `json:"id"`
	Name        string                     `json:"name"`
	Description string                     `json:"description"`
	Photos      []*sanitizedFolderPhoto    `json:"photos"`
	Project     *response.RelationshipData `json:"project"`
	CreatedAt   time.Time                  `json:"created_at"`
	UpdatedAt   time.Time                  `json:"updated_at"`
}

type sanitizedFolderPhoto struct {
	ID           string `json:"id"`
	URL          string `json:"url"`
	ThumbnailURL string `json:"-"`
}

type folderResponse struct {
	Data *sanitizedFolder `json:"data"`
}

type foldersResponse struct {
	response.PaginationData
	Data []*sanitizedFolder `json:"data"`
}

type folderPhotosResponse struct {
	Data []*adioo.Photo `json:"data"`
}

func sanitizeFolder(p *adioo.Folder, e adioo.EncryptionService) (*sanitizedFolder, error) {
	photos := []*sanitizedFolderPhoto{}
	if p.Photos != nil {
		for _, f := range p.Photos {
			fullURL, err := e.EncryptURL(f.URL)
			if err != nil {
				return nil, err
			}
			pf := &sanitizedFolderPhoto{ID: f.ID, URL: fullURL, ThumbnailURL: f.ThumbnailURL}
			photos = append(photos, pf)
		}
	}
	return &sanitizedFolder{
		ID: p.ID,
		Project: &response.RelationshipData{
			ID:   p.ProjectID,
			HREF: fmt.Sprintf("/projects/%s", p.ProjectID),
		},
		Name:        p.Name,
		Description: p.Description,
		Photos:      photos,
		CreatedAt:   p.CreatedAt,
		UpdatedAt:   p.UpdatedAt,
	}, nil
}

func sanitizeFolders(s []*adioo.Folder, e adioo.EncryptionService) ([]*sanitizedFolder, error) {
	ss := make([]*sanitizedFolder, len(s))
	for x, u := range s {
		nu, err := sanitizeFolder(u, e)
		if err != nil {
			return nil, err
		}
		ss[x] = nu
	}

	return ss, nil
}
