package httpfolder

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"
)

func TestCreateFolderRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *createFolderRequest
		expected bool
	}{
		{&createFolderRequest{Name: "testname", ProjectID: mock.TestID}, true},
		{&createFolderRequest{Name: "testname", ProjectID: "invalid uuid"}, false},
		{&createFolderRequest{Name: "testname"}, false},
		{&createFolderRequest{}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}
