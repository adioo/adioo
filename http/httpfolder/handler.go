package httpfolder

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	FolderStorage     adioo.FolderService
	EncryptionService adioo.EncryptionService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(s adioo.FolderService, e adioo.EncryptionService) *Handler {
	return &Handler{
		Router:            mux.NewRouter(),
		FolderStorage:     s,
		EncryptionService: e,
	}
}
