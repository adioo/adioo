package httpfolder

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badFolderRequest struct {
	Name int `json:"name"`
}

func TestHandleCreateFolder(t *testing.T) {
	tests := []struct {
		payload   interface{}
		projectID string
		respCode  int
	}{
		{&createFolderRequest{Name: "testname"}, mock.TestID, http.StatusCreated},
		{&createFolderRequest{Name: ""}, mock.TestID, http.StatusBadRequest},
		{&badFolderRequest{Name: 1}, mock.TestID, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/projects/%s/folders", test.projectID)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/projects/{project_id}/folders", middleware.GenerateRequestID(testHandler.HandleCreateFolder())).Methods("POST")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}

func TestHandleListProjectFolders(t *testing.T) {
	tests := []struct {
		projectID string
		respCode  int
	}{
		{mock.TestID, http.StatusOK},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/projects/%s/folders", test.projectID)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/projects/{project_id}/folders", middleware.GenerateRequestID(testHandler.HandleListProjectFolders())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}
}

func TestHandleGetFolders(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invaliduuid", http.StatusBadRequest},
		{mock.WrongTestID, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/folders/%s", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/folders/{id}", middleware.GenerateRequestID(testHandler.HandleGetFolder())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}
}
