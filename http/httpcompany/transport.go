package httpcompany

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreateCompany handles the transport of creating a new company.
func (h *Handler) HandleCreateCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createCompanyRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewCompany{
			Name:        payload.Name,
			PhoneNumber: payload.PhoneNumber,
			Email:       payload.Email,
			Address:     payload.Address,
			State:       payload.State,
			ZipCode:     payload.ZipCode,
		}

		c, err := h.CompanyStorage.Create(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := companyResponse{
			Data: sanitizeCompany(c),
		}

		response.Created(w, resp)
	})
}

// HandleGetCompany handles the transport of getting a company.
func (h *Handler) HandleGetCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		c, err := h.CompanyStorage.Company(id)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := companyResponse{
			Data: sanitizeCompany(c),
		}

		response.OK(w, resp)
	})
}

// HandleUpdateCompany handles the transport of updating a company.
func (h *Handler) HandleUpdateCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(updateCompanyRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		id := mux.Vars(r)["id"]
		payload.ID = id

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		u := &adioo.UpdateCompany{
			ID:          payload.ID,
			Name:        payload.Name,
			PhoneNumber: payload.PhoneNumber,
			Email:       payload.Email,
			Address:     payload.Address,
			State:       payload.State,
			ZipCode:     payload.ZipCode,
		}

		c, err := h.CompanyStorage.Update(u)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := companyResponse{
			Data: sanitizeCompany(c),
		}

		response.OK(w, resp)
	})
}
