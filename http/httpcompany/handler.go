package httpcompany

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	CompanyStorage adioo.CompanyService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(c adioo.CompanyService) *Handler {
	return &Handler{
		Router:         mux.NewRouter(),
		CompanyStorage: c,
	}
}
