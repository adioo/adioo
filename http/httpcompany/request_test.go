package httpcompany

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCreateCompanyRequestValidate(t *testing.T) {
	tests := []struct {
		req      *createCompanyRequest
		expected bool
	}{
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, true},
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "Florida", ZipCode: 55555}, false},
		{&createCompanyRequest{Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, false},
		{&createCompanyRequest{Name: "testname", Email: "email@email.com"}, true},
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", Address: "my addr", State: "FL", ZipCode: 55555}, true},
		{&createCompanyRequest{Name: "testname", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, true},
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", State: "FL", ZipCode: 55555}, true},
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", ZipCode: 55555}, true},
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL"}, true},
		{&createCompanyRequest{Name: "testname", Email: "tst.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, false},
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", PhoneNumber: "6767", Address: "my addr", State: "FL", ZipCode: 55555}, false},
		{&createCompanyRequest{Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 5555}, false},
		{&createCompanyRequest{Name: "testname", Address: "my addr", State: "FL", ZipCode: 55555}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, result, test.expected)
	}
}

func TestUpdateCompanyRequestValidate(t *testing.T) {
	id := uuid.NewV4().String()
	tests := []struct {
		req      *updateCompanyRequest
		expected bool
	}{
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, true},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "Florida", ZipCode: 55555}, false},
		{&updateCompanyRequest{ID: id, Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, false},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "email@email.com"}, true},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", Address: "my addr", State: "FL", ZipCode: 55555}, true},
		{&updateCompanyRequest{ID: id, Name: "testname", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, true},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", State: "FL", ZipCode: 55555}, true},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", ZipCode: 55555}, true},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL"}, true},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "tst.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, false},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", PhoneNumber: "6767", Address: "my addr", State: "FL", ZipCode: 55555}, false},
		{&updateCompanyRequest{ID: id, Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 5555}, false},
		{&updateCompanyRequest{ID: id, Name: "testname", Address: "my addr", State: "FL", ZipCode: 55555}, false},
		{&updateCompanyRequest{ID: "wrongid", Name: "testname", Email: "t@test.co", PhoneNumber: "9654446767", Address: "my addr", State: "FL", ZipCode: 55555}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}
