package httpcompany

import (
	"fmt"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type companyResponse struct {
	Data *sanitizedCompany `json:"data"`
}

type sanitizedCompany struct {
	ID          string                      `json:"id"`
	Name        string                      `json:"string"`
	PhoneNumber string                      `json:"phone_number"`
	Email       string                      `json:"email"`
	Address     string                      `json:"address"`
	State       string                      `json:"state"`
	ZipCode     int                         `json:"zip_code"`
	Employees   *response.RelationshipsData `json:"employees"`
	Customers   *response.RelationshipsData `json:"customers"`
	CreatedAt   time.Time                   `json:"created_at"`
	UpdatedAt   time.Time                   `json:"updated_at"`
}

func sanitizeCompany(c *adioo.Company) *sanitizedCompany {
	return &sanitizedCompany{
		ID:          c.ID,
		Name:        c.Name,
		PhoneNumber: c.PhoneNumber,
		Email:       c.Email,
		Address:     c.Address,
		State:       c.State,
		ZipCode:     c.ZipCode,
		Employees: &response.RelationshipsData{
			HREF: fmt.Sprintf("/companies/%s/employees", c.ID),
		},
		Customers: &response.RelationshipsData{
			HREF: fmt.Sprintf("/companies/%s/customers", c.ID),
		},
		CreatedAt: c.CreatedAt,
		UpdatedAt: c.UpdatedAt,
	}
}
