package httpcompany

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badCompanyRequest struct {
	Name int
}

func TestHandleCreateCompany(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{createCompanyRequest{Name: mock.TestCompanyName, Email: "m@test.com", PhoneNumber: "1233331111", Address: "test", State: "as", ZipCode: 33244}, http.StatusCreated},
		{createCompanyRequest{Name: "incorrect name", Email: "m@test.com", PhoneNumber: "1233331111", Address: "test", State: "as", ZipCode: 33244}, http.StatusInternalServerError},
		{createCompanyRequest{Name: "test"}, http.StatusBadRequest},
		{&badCompanyRequest{Name: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", "/companies", bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies", middleware.GenerateRequestID(testHandler.HandleCreateCompany()))

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}

func TestHandleUpdateCompany(t *testing.T) {
	tests := []struct {
		payload  interface{}
		id       string
		respCode int
	}{
		{updateCompanyRequest{Name: mock.TestCompanyName, Email: "m@test.com", PhoneNumber: "1233331111", Address: "test", State: "as", ZipCode: 33244}, mock.TestID, http.StatusOK},
		{updateCompanyRequest{Name: mock.TestCompanyName, Email: "m@test.com", PhoneNumber: "1233331111", Address: "test", State: "as", ZipCode: 33244}, "notuuuid", http.StatusBadRequest},
		{updateCompanyRequest{Name: mock.TestCompanyName, Email: "m@test.com", PhoneNumber: "1233331111", Address: "test", State: "as", ZipCode: 33244}, uuid.NewV4().String(), http.StatusNotFound},
		{updateCompanyRequest{Name: mock.TestCompanyName}, mock.TestID, http.StatusBadRequest},
		{&badCompanyRequest{Name: 1}, mock.TestID, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/companies/%s", test.id)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies/{id}", middleware.GenerateRequestID(testHandler.HandleUpdateCompany())).Methods("PUT")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}
func TestGetCompany(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invaliduuid", http.StatusBadRequest},
		{uuid.NewV4().String(), http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/companies/%s", test.id)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/companies/{id}", middleware.GenerateRequestID(testHandler.HandleGetCompany())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}
}
