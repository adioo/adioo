package httpcompany

import (
	"errors"
	"strconv"

	"github.com/asaskevich/govalidator"
)

type createCompanyRequest struct {
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number"`
	Email       string `json:"email"`
	Address     string `json:"address"`
	State       string `json:"state"`
	ZipCode     int    `json:"zip_code"`
}

func (r *createCompanyRequest) Validate() error {
	switch {
	case r.Name == "":
		return errors.New("name is required")
	case r.PhoneNumber == "" && r.Email == "":
		return errors.New("phone number or email address is required")
	}
	if r.Email != "" {
		if !govalidator.IsEmail(r.Email) {
			return errors.New("email is invalid")
		}
	}
	if r.PhoneNumber != "" {
		if len(r.PhoneNumber) != 10 {
			return errors.New("phone number is invalid. Only provide numeric characters. Ex. 9544445555")
		}
	}
	if r.ZipCode != 0 {
		if len(strconv.Itoa(r.ZipCode)) != 5 {
			return errors.New("invalid zip code")
		}
	}
	if r.State != "" {
		if len(r.State) != 2 {
			return errors.New("please provide short code for state. Ex Florida -> FL")
		}
	}
	return nil
}

type updateCompanyRequest struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number"`
	Email       string `json:"email"`
	Address     string `json:"address"`
	State       string `json:"state"`
	ZipCode     int    `json:"zip_code"`
}

func (r *updateCompanyRequest) Validate() error {
	switch {
	case !govalidator.IsUUIDv4(r.ID):
		return errors.New("invalid company id")
	case r.Name == "":
		return errors.New("name is required")
	case r.PhoneNumber == "" && r.Email == "":
		return errors.New("phone number or email address is required")
	}
	if r.Email != "" {
		if !govalidator.IsEmail(r.Email) {
			return errors.New("email is invalid")
		}
	}
	if r.PhoneNumber != "" {
		if len(r.PhoneNumber) != 10 {
			return errors.New("phone number is invalid. Only provide numeric characters. Ex. 9544445555")
		}
	}
	if r.ZipCode != 0 {
		if len(strconv.Itoa(r.ZipCode)) != 5 {
			return errors.New("invalid zip code")
		}
	}
	if r.State != "" {
		if len(r.State) != 2 {
			return errors.New("please provide short code for state. Ex Florida -> FL")
		}
	}
	return nil
}
