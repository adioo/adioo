package http

import (
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"time"
)

// ListenAndServe just copies the http package ListenAndServe function
// later on I should refactor this to actually create my own server and handlers for now yolo
func ListenAndServe(addr string, handler http.Handler) error {
	return http.ListenAndServe(addr, handler)
}

// ListenAndServeTLS just copies the http package ListenAndServe function
// later on I should refactor this to actually create my own server and handlers for now yolo
func ListenAndServeTLS(addr, certFile, keyFile string, handler http.Handler) error {
	return http.ListenAndServeTLS(addr, certFile, keyFile, handler)
}

// Serve is
func Serve(handler http.Handler) error {
	// Set the httpAddress
	httpAddress := ":8000"
	if os.Getenv("PORT") != "" {
		httpAddress = ":" + os.Getenv("PORT")
	}

	if os.Getenv("HTTP") == "true" {
		srv := &http.Server{
			ReadTimeout:  15 * time.Second,
			WriteTimeout: 15 * time.Second,
			IdleTimeout:  120 * time.Second,
			Handler:      handler,
			Addr:         httpAddress,
		}
		log.Println("Starting server on port", httpAddress)
		return srv.ListenAndServe()
	}

	httpAddress = ":443"
	if os.Getenv("SSL_PORT") != "" {
		httpAddress = ":" + os.Getenv("SSL_PORT")
	}

	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      http.HandlerFunc(redirectTLS),
	}
	go func() { log.Fatal(srv.ListenAndServe()) }()

	tlsConfig := &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519, // Go 1.8 only
		},
	}

	tlsSrv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		TLSConfig:    tlsConfig,
		Handler:      handler,
		Addr:         httpAddress,
	}
	log.Println("Starting server on port", httpAddress)
	return tlsSrv.ListenAndServeTLS(os.Getenv("SSL_CERT_PATH"), os.Getenv("SSL_KEY_PATH"))
}
