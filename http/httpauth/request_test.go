package httpauth

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/adioo/adioo/mock"
)

func TestAuthRequestValidate(t *testing.T) {
	tests := []struct {
		req      *authenticateRequest
		expected bool
	}{
		{&authenticateRequest{Email: mock.TestEmail, Password: "password"}, true},
		{&authenticateRequest{Email: mock.TestEmail}, false},
		{&authenticateRequest{Password: "password"}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}
