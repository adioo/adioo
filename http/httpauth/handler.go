package httpauth

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	EmployeeStorage  adioo.EmployeeService
	BlacklistService adioo.BlacklistStorage
	AuthService      adioo.AuthService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(e adioo.EmployeeService, b adioo.BlacklistStorage, a adioo.AuthService) *Handler {
	return &Handler{
		Router:           mux.NewRouter(),
		EmployeeStorage:  e,
		BlacklistService: b,
		AuthService:      a,
	}
}
