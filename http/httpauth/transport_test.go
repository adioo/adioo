package httpauth

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badAuthRequest struct {
	Email int
}

func TestHandleAuthenticate(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&authenticateRequest{Email: mock.TestEmail, Password: "password"}, 200},
		{&authenticateRequest{Email: "weongemail@email.com", Password: "password"}, 404},
		{&authenticateRequest{Email: mock.TestEmail}, 400},
		{&authenticateRequest{Password: "password"}, 400},
		{&badAuthRequest{Email: 1}, 400},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", "/authenticate", bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/authenticate", middleware.GenerateRequestID(testHandler.HandleAuthenticate()))

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}

func TestHandleLogout(t *testing.T) {
	tests := []struct {
		respCode int
	}{
		{200},
	}

	for _, test := range tests {
		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", "/logout", nil)
		if err != nil {
			t.Fatal(err)
		}

		k := middleware.TokenContextKey("token")
		ctx := context.WithValue(req.Context(), k, testJWT)

		rr := httptest.NewRecorder()
		testRouter.Handle("/logout", middleware.GenerateRequestID(testHandler.HandleLogout())).Methods("GET")

		testRouter.ServeHTTP(rr, req.WithContext(ctx))

		assert.Equal(t, test.respCode, rr.Code)
	}

}
