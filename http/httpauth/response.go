package httpauth

type authenticateResponse struct {
	Data *authResponseData `json:"data"`
}

type authResponseData struct {
	Token string `json:"token"`
}
