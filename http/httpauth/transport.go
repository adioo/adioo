package httpauth

import (
	"encoding/json"
	"fmt"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleAuthenticate is
func (h *Handler) HandleAuthenticate() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(authenticateRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		e, err := h.EmployeeStorage.Authenticate(payload.Email, payload.Password)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		// clean this method up return. Iffy about returning 3 values
		token, err := h.AuthService.Token(e.ID, e.CompanyID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := authenticateResponse{
			Data: &authResponseData{token},
		}

		response.OK(w, resp)
	})
}

// HandleLogout is
func (h *Handler) HandleLogout() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := middleware.GetReqToken(r)
		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			response.WithError(w, adioo.ErrInvalidJWT, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}
		jti := claims["jti"].(string)
		exp := claims["exp"].(float64)
		fmt.Println("exp", int64(exp))
		bl := &adioo.Blacklist{
			JTI:     jti,
			Expires: int64(exp),
		}

		if err := h.BlacklistService.Create(bl); err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := response.Message{
			Data: &response.MessageData{Message: "successfully logged out"},
		}

		response.OK(w, resp)
	})
}
