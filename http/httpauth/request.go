package httpauth

import "errors"

type authenticateRequest struct {
	Email    string
	Password string
}

func (r *authenticateRequest) Validate() error {
	switch {
	case r.Email == "":
		return errors.New("email is required")
	case r.Password == "":
		return errors.New("password is required")
	}
	return nil
}
