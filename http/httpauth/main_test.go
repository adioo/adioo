package httpauth

import (
	"os"
	"testing"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"

	"gitlab.com/adioo/adioo/mock"
)

var testHandler *Handler
var testRouter *mux.Router
var testJWT *jwt.Token

func TestMain(m *testing.M) {
	testHandler = NewHandler(&mock.EmployeeService{}, &mock.BlacklistStorage{}, &mock.AuthService{})
	testRouter = mux.NewRouter()

	testJWT = createTestJWT()

	t := m.Run()

	os.Exit(t)
}

func createTestJWT() *jwt.Token {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["sub"] = mock.TestID
	claims["iat"] = time.Now().Unix()
	claims["exp"] = float64(time.Now().Add(time.Hour * 720).Unix()) // Expire in 30 days
	claims["nbf"] = time.Now().Unix()
	claims["iss"] = os.Getenv("BASE_URL")
	claims["jti"] = "testjit"
	return token
}
