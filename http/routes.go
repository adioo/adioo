package http

import "gitlab.com/adioo/adioo/http/middleware"

func (h *Handler) routes(authMiddleware *middleware.Auth) {
	// HEALTH ROUTES
	const healthPath = "/health"
	h.Handle(healthPath,
		h.handleHealth()).Methods("GET")

	// AUTH ROUTES
	const authenticatePath = "/authenticate"
	h.Handle(authenticatePath,
		h.AuthHandler.HandleAuthenticate()).Methods("POST")

	const logoutPath = "/logout"
	h.Handle(logoutPath,
		authMiddleware.Authentication(
			h.AuthHandler.HandleLogout())).Methods("GET")

	// COMPANY ROUTES
	const createCompanyPath = "/companies"
	h.Handle(createCompanyPath,
		h.CompanyHandler.HandleCreateCompany()).Methods("POST")

	const updateCompanyPath = "/companies/{id}"
	h.Handle(updateCompanyPath,
		h.CompanyHandler.HandleUpdateCompany()).Methods("PUT")

	const getCompanyPath = "/companies/{id}"
	h.Handle(getCompanyPath,
		authMiddleware.Authentication(
			h.CompanyHandler.HandleGetCompany())).Methods("GET")

	// EMPLOYEE ROUTES
	const createEmployeePath = "/employees"
	h.Handle(createEmployeePath,
		h.EmployeeHandler.HandleCreateEmployee()).Methods("POST")

	const updateEmployeePath = "/employees/{id}"
	h.Handle(updateEmployeePath,
		h.EmployeeHandler.HandleUpdateEmployee()).Methods("PUT")

	const getCompanyEmployeesPath = "/companies/{company_id}/employees"
	h.Handle(getCompanyEmployeesPath,
		authMiddleware.Authentication(
			authMiddleware.Authorization(
				h.EmployeeHandler.HandleListCompanyEmployees(), "employee"))).Methods("GET")

	const getEmployeePath = "/employees/{id}"
	h.Handle(getEmployeePath,
		authMiddleware.Authentication(
			authMiddleware.Authorization(
				h.EmployeeHandler.HandleGetEmployee(), "employee"))).Methods("GET")

	// CUSTOMER ROUTES
	const createCustomerPath = "/customers"
	h.Handle(createCustomerPath,
		authMiddleware.Authentication(
			authMiddleware.Authorization(
				h.CustomerHandler.HandleCreateCustomer(), "customer"))).Methods("POST")

	const getCustomerPath = "/customers/{id}"
	h.Handle(getCustomerPath,
		authMiddleware.Authentication(
			authMiddleware.Authorization(
				h.CustomerHandler.HandleGetCustomer(), "customer"))).Methods("GET")

	const getCompanyCustomersPath = "/companies/{company_id}/customers"
	h.Handle(getCompanyCustomersPath,
		authMiddleware.Authentication(
			authMiddleware.Authorization(
				h.CustomerHandler.HandleListCompanyCustomers(), "customer"))).Methods("GET")

	// PROJECT ROUTES
	const createProjectPath = "/projects"
	h.Handle(createProjectPath,
		authMiddleware.Authentication(
			h.ProjectHandler.HandleCreateProject())).Methods("POST")

	const createProjectCategoryPath = "/categories"
	h.Handle(createProjectCategoryPath,
		authMiddleware.Authentication(
			h.ProjectHandler.HandleCreateProjectCategory())).Methods("POST")

	const getProjectPath = "/projects/{id}"
	h.Handle(getProjectPath,
		authMiddleware.Authentication(
			h.ProjectHandler.HandleGetProject())).Methods("GET")

	const updateProjectPath = "/projects/{id}"
	h.Handle(updateProjectPath,
		authMiddleware.Authentication(
			h.ProjectHandler.HandleUpdateProject())).Methods("PUT")

	// COMPANY ROUTES
	const listCompanyProjectsPath = "/companies/{company_id}/projects"
	h.Handle(listCompanyProjectsPath,
		authMiddleware.Authentication(
			h.ProjectHandler.HandleListCompanyProjects())).Methods("GET")

	const listCompanyCategoriesPath = "/companies/{company_id}/categories"
	h.Handle(listCompanyCategoriesPath,
		authMiddleware.Authentication(
			h.ProjectHandler.HandleListCompanyCategories())).Methods("GET")

	// GALLERY ROUTES
	const createGalleryPath = "/galleries"
	h.Handle(createGalleryPath,
		authMiddleware.Authentication(
			h.GalleryHandler.HandleCreateGallery())).Methods("POST")

	const getGalleryPath = "/galleries/{id}"
	h.Handle(getGalleryPath,
		h.GalleryHandler.HandleGetGallery()).Methods("GET")

	const listEmployeeGalleriesPath = "/employees/{employee_id}/galleries"
	h.Handle(listEmployeeGalleriesPath,
		authMiddleware.Authentication(
			h.GalleryHandler.HandleListEmployeeGalleries())).Methods("GET")

	const listCompanyGalleriesPath = "/companies/{company_id}/galleries"
	h.Handle(listCompanyGalleriesPath,
		authMiddleware.Authentication(
			h.GalleryHandler.HandleListCompanyGalleries())).Methods("GET")

	const createGalleryPhotoCommentPath = "/galleries/{gallery_id}/photos/{photo_id}/comments"
	h.Handle(createGalleryPhotoCommentPath,
		h.GalleryHandler.HandleCreateGalleryPhotoComment()).Methods("POST")

	const listGalleryPhotoCommentsPath = "/galleries/{gallery_id}/photos/{photo_id}/comments"
	h.Handle(listGalleryPhotoCommentsPath,
		h.GalleryHandler.HandleListGalleryPhotoComments()).Methods("GET")

	const listGalleryPhotosPath = "/galleries/{id}/photos"
	h.Handle(listGalleryPhotosPath,
		h.GalleryHandler.HandleListGalleryPhotos()).Methods("GET")

	const addGalleryPhotoPath = "/galleries/{gallery_id}/photos"
	h.Handle(addGalleryPhotoPath,
		authMiddleware.Authentication(
			h.GalleryHandler.HandleAddGalleryPhoto())).Methods("POST")

	const getGalleryPhotoPath = "/galleries/{gallery_id}/photos/{photo_id}"
	h.Handle(getGalleryPhotoPath,
		h.GalleryHandler.HandleGetGalleryPhoto()).Methods("GET")

	const removeGalleryPhotoPath = "/galleries/{gallery_id}/photos/{photo_id}"
	h.Handle(removeGalleryPhotoPath,
		authMiddleware.Authentication(
			h.GalleryHandler.HandleRemoveGalleryPhoto())).Methods("DELETE")

	// FOLDER ROUTES
	const createFolderPath = "/projects/{project_id}/folders"
	h.Handle(createFolderPath,
		authMiddleware.Authentication(
			h.FolderHandler.HandleCreateFolder())).Methods("POST")

	const getFolderPath = "/folders/{id}"
	h.Handle(getFolderPath,
		authMiddleware.Authentication(
			h.FolderHandler.HandleGetFolder())).Methods("GET")

	const listProjectFoldersPath = "/projects/{project_id}/folders"
	h.Handle(listProjectFoldersPath,
		authMiddleware.Authentication(
			h.FolderHandler.HandleListProjectFolders())).Methods("GET")

	// EVENT ROUTES
	const createEventPath = "/events"
	h.Handle(createEventPath,
		authMiddleware.Authentication(
			h.EventHandler.HandleCreateEvent())).Methods("POST")

	const listEmployeeEventsPath = "/employees/{employee_id}/events"
	h.Handle(listEmployeeEventsPath,
		authMiddleware.Authentication(
			h.EventHandler.HandleListEmployeeEvents())).Methods("GET")

	// PHOTO ROUTES
	const createPhotoPath = "/photos"
	h.Handle(createPhotoPath,
		authMiddleware.Authentication(
			h.PhotoHandler.HandleCreatePhoto())).Methods("POST")

	const listProjectPhotosPath = "/projects/{project_id}/photos"
	h.Handle(listProjectPhotosPath,
		authMiddleware.Authentication(
			h.PhotoHandler.HandleListProjectPhotos())).Methods("GET")

	const deletePhotoPath = "/photos/{id}"
	h.Handle(deletePhotoPath,
		authMiddleware.Authentication(
			h.PhotoHandler.HandleRemovePhoto())).Methods("DELETE")

	const addFolderPhotoPath = "/folders/{folder_id}/photos/{photo_id}"
	h.Handle(addFolderPhotoPath,
		authMiddleware.Authentication(
			h.PhotoHandler.HandleAddFolderPhoto())).Methods("PUT")

	const removeFolderPhotoPath = "/folders/{folder_id}/photos/{photo_id}"
	h.Handle(removeFolderPhotoPath,
		authMiddleware.Authentication(
			h.PhotoHandler.HandleRemoveFolderPhoto())).Methods("DELETE")
}
