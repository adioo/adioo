package httpevent

import (
	"os"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo/mock"
)

var testHandler *Handler
var testRouter *mux.Router

func TestMain(m *testing.M) {
	testHandler = NewHandler(&mock.EventService{})
	testRouter = mux.NewRouter()

	t := m.Run()

	os.Exit(t)
}
