package httpevent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badEventRequest struct {
	Title int `json:"title"`
}

func TestHandleCreateCustomer(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: mock.TestID, Title: "t", Start: "1234567890", End: "1234567890"}, http.StatusCreated},
		{&createEventRequest{}, http.StatusBadRequest},
		{&badEventRequest{Title: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", "/events", bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/events", middleware.GenerateRequestID(testHandler.HandleCreateEvent()))

		testRouter.ServeHTTP(rr, req)

		assert.Equal(t, test.respCode, rr.Code)
	}

}

func TestHandleListEmployeeEvents(t *testing.T) {
	tests := []struct {
		id       string
		count    string
		start    string
		end      string
		respCode int
	}{
		{mock.TestID, "", "", "", http.StatusOK},
		{"invalid uuid", "", "", "", http.StatusBadRequest},
		{mock.TestID, "20", "", "", http.StatusOK},
		{mock.TestID, "", "1234567890", "12", http.StatusBadRequest},
		{mock.TestID, "", "12", "1234567890", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/employees/%s/events", test.id)
		if test.count != "" {
			url += fmt.Sprintf("?count=%s", test.count)
		}
		if test.start != "" {
			url += fmt.Sprintf("?start=%s", test.start)
		}
		if test.end != "" {
			url += fmt.Sprintf("&end=%s", test.end)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/employees/{employee_id}/events", middleware.GenerateRequestID(testHandler.HandleListEmployeeEvents())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			fmt.Println(url)
		}
	}

}
