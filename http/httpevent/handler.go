package httpevent

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	EventStorage adioo.EventService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(c adioo.EventService) *Handler {
	return &Handler{
		Router:       mux.NewRouter(),
		EventStorage: c,
	}
}
