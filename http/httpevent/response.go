package httpevent

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type eventResponse struct {
	Data *sanitizedEvent `json:"data"`
}

type eventsResponse struct {
	response.PaginationData
	Data []*sanitizedEvent `json:"data"`
}

type sanitizedEvent struct {
	ID          string                     `json:"id"`
	Title       string                     `json:"title"`
	Description string                     `json:"description"`
	Start       string                     `json:"start"`
	End         string                     `json:"end"`
	Location    string                     `json:"location"`
	Notes       string                     `json:"notes"`
	Employee    *response.RelationshipData `json:"employee"`
	Customer    *response.RelationshipData `json:"customer"`
	CreatedAt   time.Time                  `json:"created_at"`
	UpdatedAt   time.Time                  `json:"updated_at"`
}

func sanitizeEvent(e *adioo.Event) *sanitizedEvent {
	var customerRelationshipData *response.RelationshipData
	if e.CustomerID != "" {
		customerRelationshipData = new(response.RelationshipData)
		customerRelationshipData.ID = e.CustomerID
		customerRelationshipData.HREF = fmt.Sprintf("/customers/%s", e.CustomerID)
	}
	return &sanitizedEvent{
		ID: e.ID,
		Employee: &response.RelationshipData{
			ID:   e.EmployeeID,
			HREF: fmt.Sprintf("/employees/%s", e.EmployeeID),
		},
		Customer:    customerRelationshipData,
		Title:       e.Title,
		Description: e.Description,
		Start:       strconv.Itoa(int(e.Start.Unix())),
		End:         strconv.Itoa(int(e.End.Unix())),
		Location:    e.Location,
		Notes:       e.Notes,
		CreatedAt:   e.CreatedAt,
		UpdatedAt:   e.UpdatedAt,
	}
}

func sanitizeEvents(s []*adioo.Event) []*sanitizedEvent {
	ss := make([]*sanitizedEvent, len(s))
	for x, u := range s {
		nu := sanitizeEvent(u)
		ss[x] = nu
	}

	return ss
}
