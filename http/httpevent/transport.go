package httpevent

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreateEvent handles the transport for creating a new event
func (h *Handler) HandleCreateEvent() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createEventRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewEvent{
			EmployeeID:  payload.EmployeeID,
			CustomerID:  payload.CustomerID,
			Title:       payload.Title,
			Description: payload.Description,
			Start:       payload.Start,
			End:         payload.End,
			Location:    payload.Location,
			Notes:       payload.Notes,
		}

		e, err := h.EventStorage.Create(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := eventResponse{
			Data: sanitizeEvent(e),
		}

		response.Created(w, resp)
	})
}

// HandleListEmployeeEvents handles the transport for listing employee events
func (h *Handler) HandleListEmployeeEvents() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["employee_id"]
		if !govalidator.IsUUIDv4(id) {
			response.WithError(w, errors.New("invalid id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		q := r.URL.Query()
		start := q.Get("start")
		end := q.Get("end")
		limit := 20
		if q.Get("count") != "" {
			ilimit, err := strconv.Atoi(q.Get("count"))
			if err != nil {
				response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
				return
			}
			limit = ilimit
		}
		sinceID := q.Get("since_id")

		if start == "" || end == "" {
			start = strconv.FormatInt(time.Now().Add(-720*time.Hour).Unix(), 10)
			end = strconv.FormatInt(time.Now().Add(720*time.Hour).Unix(), 10)
		}

		if len(start) != 10 {
			response.WithError(w, errors.New("invalid start time"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if len(end) != 10 {
			response.WithError(w, errors.New("invalid end time"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		e, err := h.EventStorage.ListEmployeeEvents(id, start, end, sinceID, limit)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := eventsResponse{
			Data: sanitizeEvents(e),
		}
		eLen := len(e)
		resp.Count = limit
		if eLen < limit {
			resp.Count = eLen
		}
		if eLen == limit {
			resp.SinceID = e[eLen-1].ID
			resp.Links.Next = fmt.Sprintf("/employees/%s/events?since_id=%s", id, resp.SinceID)
		}
		resp.Links.Self = r.URL.String()

		response.OK(w, resp)
	})
}
