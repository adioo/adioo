package httpevent

import (
	"errors"

	"github.com/asaskevich/govalidator"
)

// createEventRequest
type createEventRequest struct {
	EmployeeID  string `json:"employee_id"`
	CustomerID  string `json:"customer_id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Start       string `json:"start"`
	End         string `json:"end"`
	Location    string `json:"location"`
	Notes       string `json:"notes"`
}

func (r *createEventRequest) Validate() error {
	switch {
	case r.EmployeeID == "":
		return errors.New("employee id is required")
	case !govalidator.IsUUIDv4(r.EmployeeID):
		return errors.New("invalid employee id")
	case r.CustomerID != "" && !govalidator.IsUUIDv4(r.CustomerID):
		return errors.New("invalid customer id")
	case r.Title == "":
		return errors.New("title is required")
	case r.Start == "":
		return errors.New("start is required")
	case r.Start != "" && len(r.Start) != 10:
		return errors.New("invalid start timestamp")
	case r.End == "":
		return errors.New("end is required")
	case r.End != "" && len(r.End) != 10:
		return errors.New("invalid end timestamp")
	}

	return nil
}
