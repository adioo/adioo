package httpevent

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"
)

func TestCreateEventRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *createEventRequest
		expected bool
	}{
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: mock.TestID, Title: "t", Start: "1234567890", End: "1234567890"}, true},
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: mock.TestID, Title: "t", Start: "1234567890", End: "1"}, false},
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: mock.TestID, Title: "t", Start: "1", End: "1234567890"}, false},
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: mock.TestID, Title: "t", Start: "1234567890"}, false},
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: mock.TestID, Title: "t", End: "1234567890"}, false},
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: mock.TestID, Start: "1234567890", End: "1234567890"}, false},
		{&createEventRequest{EmployeeID: mock.TestID, CustomerID: "invaliduuid", Title: "t", Start: "1234567890", End: "1234567890"}, false},
		{&createEventRequest{EmployeeID: "invaliduuid", CustomerID: mock.TestID, Title: "t", Start: "1234567890", End: "1234567890"}, false},
		{&createEventRequest{CustomerID: mock.TestID, Title: "t", Start: "1234567890", End: "1234567890"}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, result, test.expected)
	}
}
