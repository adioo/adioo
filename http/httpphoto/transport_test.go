package httpphoto

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/mock"
)

type badPhotoRequest struct {
	ProjectID int `json:"project_id"`
}

func TestHandleCreatePhoto(t *testing.T) {
	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{&createPhotoRequest{ProjectID: mock.TestID, URL: mock.TestURL}, http.StatusCreated},
		{&createPhotoRequest{URL: mock.TestURL}, http.StatusBadRequest},
		{&badPhotoRequest{ProjectID: 1}, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := "/photos"

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Fatal(err)
		}

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/photos", middleware.GenerateRequestID(testHandler.HandleCreatePhoto())).Methods("POST")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandleListProjectPhotos(t *testing.T) {
	tests := []struct {
		id       string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invaliduuid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/projects/%s/photos", test.id)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/projects/{project_id}/photos", middleware.GenerateRequestID(testHandler.HandleListProjectPhotos())).Methods("GET")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandlAddFolderPhoto(t *testing.T) {
	tests := []struct {
		folderID string
		photoID  string
		respCode int
	}{
		{mock.TestID, mock.TestID, http.StatusCreated},
		{"invalid", mock.TestID, http.StatusBadRequest},
		{mock.TestID, "invalid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/folders/%s/photos/%s", test.folderID, test.photoID)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("PUT", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/folders/{folder_id}/photos/{photo_id}", middleware.GenerateRequestID(testHandler.HandleAddFolderPhoto())).Methods("PUT")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}

}

func TestHandlRemoveFolderPhoto(t *testing.T) {
	tests := []struct {
		folderID string
		photoID  string
		respCode int
	}{
		{mock.TestID, mock.TestID, http.StatusOK},
		{"invalid", mock.TestID, http.StatusBadRequest},
		{mock.TestID, "invalid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/folders/%s/photos/%s", test.folderID, test.photoID)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/folders/{folder_id}/photos/{photo_id}", middleware.GenerateRequestID(testHandler.HandleRemoveFolderPhoto())).Methods("DELETE")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}

func TestHandlRemovePhoto(t *testing.T) {
	tests := []struct {
		photoID  string
		respCode int
	}{
		{mock.TestID, http.StatusOK},
		{"invalid", http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("/photos/%s", test.photoID)

		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		testRouter.Handle("/photos/{id}", middleware.GenerateRequestID(testHandler.HandleRemovePhoto())).Methods("DELETE")

		testRouter.ServeHTTP(rr, req)

		if !assert.Equal(t, test.respCode, rr.Code) {
			html, _ := ioutil.ReadAll(rr.Body)
			fmt.Println(string(html))
		}
	}
}
