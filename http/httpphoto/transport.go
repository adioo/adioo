package httpphoto

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandleCreatePhoto is
func (h *Handler) HandleCreatePhoto() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := new(createPhotoRequest)
		if err := json.NewDecoder(r.Body).Decode(payload); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			response.WithError(w, err, http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		n := &adioo.NewPhoto{
			ProjectID:    payload.ProjectID,
			URL:          payload.URL,
			ThumbnailURL: payload.ThumbnailURL,
		}

		p, err := h.PhotoStorage.Create(n)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		sp, err := sanitizePhoto(p, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &projectPhotoResponse{
			Data: sp,
		}

		response.Created(w, resp)
	})
}

// HandleListProjectPhotos is
func (h *Handler) HandleListProjectPhotos() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		projectID := mux.Vars(r)["project_id"]
		if !govalidator.IsUUIDv4(projectID) {
			response.WithError(w, errors.New("invalid project id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		p, err := h.PhotoStorage.ListProjectPhotos(projectID)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		if p == nil {
			p = []*adioo.Photo{}
		}

		sp, err := sanitizePhotos(p, h.EncryptionService)
		if err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := projectPhotosResponse{
			Data: sp,
		}

		response.OK(w, resp)
	})
}

// HandleAddFolderPhoto is
func (h *Handler) HandleAddFolderPhoto() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		folderID := mux.Vars(r)["folder_id"]
		photoID := mux.Vars(r)["photo_id"]

		if !govalidator.IsUUIDv4(folderID) {
			response.WithError(w, errors.New("invalid folder id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if !govalidator.IsUUIDv4(photoID) {
			response.WithError(w, errors.New("invalid photo id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := h.PhotoStorage.AddFolderPhoto(folderID, photoID); err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &response.Message{
			Data: &response.MessageData{Message: "photo was added"},
		}

		response.Created(w, resp)
	})
}

// HandleRemoveFolderPhoto is
func (h *Handler) HandleRemoveFolderPhoto() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		folderID := mux.Vars(r)["folder_id"]
		photoID := mux.Vars(r)["photo_id"]

		if !govalidator.IsUUIDv4(folderID) {
			response.WithError(w, errors.New("invalid folder id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if !govalidator.IsUUIDv4(photoID) {
			response.WithError(w, errors.New("invalid photo id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := h.PhotoStorage.RemoveFolderPhoto(folderID, photoID); err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := &response.Message{
			Data: &response.MessageData{Message: "photo was removed"},
		}

		response.OK(w, resp)
	})
}

// HandleRemovePhoto is
func (h *Handler) HandleRemovePhoto() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		photoID := mux.Vars(r)["id"]
		if !govalidator.IsUUIDv4(photoID) {
			response.WithError(w, errors.New("invalid photo id"), http.StatusBadRequest, middleware.GetReqID(r))
			return
		}

		if err := h.PhotoStorage.RemovePhoto(photoID); err != nil {
			response.WithError(w, err, http.StatusInternalServerError, middleware.GetReqID(r))
			return
		}

		resp := response.Message{
			Data: &response.MessageData{Message: "photo was removed"},
		}

		response.OK(w, resp)
	})
}
