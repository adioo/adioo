package httpphoto

import (
	"errors"
	"fmt"
	"net/url"

	"github.com/asaskevich/govalidator"
)

type createPhotoRequest struct {
	ProjectID    string `json:"project_id"`
	URL          string `json:"url"`
	ThumbnailURL string `json:"thumbnail_url"`
}

func (r *createPhotoRequest) Validate() error {
	_, urlErr := url.ParseRequestURI(r.URL)
	_, turlErr := url.ParseRequestURI(r.ThumbnailURL)
	switch {
	case !govalidator.IsUUIDv4(r.ProjectID):
		return errors.New("invalid project id")
	case r.URL == "":
		return errors.New("url is required")
	case urlErr != nil:
		return fmt.Errorf("invalid url %s", r.URL)
	case r.ThumbnailURL != "" && turlErr != nil:
		return fmt.Errorf("invalid url %s", r.ThumbnailURL)
	}
	return nil
}
