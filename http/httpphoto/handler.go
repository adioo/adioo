package httpphoto

import (
	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo"
)

// Handler represents a handler that will handle all company resource related request.
type Handler struct {
	*mux.Router
	PhotoStorage      adioo.PhotoService
	EncryptionService adioo.EncryptionService
}

// NewHandler creates a new Handler while setting defaults.
func NewHandler(s adioo.PhotoService, e adioo.EncryptionService) *Handler {
	return &Handler{
		Router:            mux.NewRouter(),
		PhotoStorage:      s,
		EncryptionService: e,
	}
}
