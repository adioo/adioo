package httpphoto

import (
	"os"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo/mock"
)

var testHandler *Handler
var testRouter *mux.Router

func TestMain(m *testing.M) {
	os.Setenv("DEBUG", "true")

	testHandler = NewHandler(&mock.PhotoService{}, &mock.EncryptionService{})
	testRouter = mux.NewRouter()

	t := m.Run()

	os.Exit(t)
}
