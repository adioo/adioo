package httpphoto

import (
	"fmt"
	"time"

	"gitlab.com/adioo/adioo"
	"gitlab.com/adioo/adioo/http/response"
)

type projectPhotoResponse struct {
	Data *sanitizedPhoto `json:"data"`
}

type projectPhotosResponse struct {
	Data []*sanitizedPhoto `json:"data"`
}

type sanitizedPhoto struct {
	ID        string                     `json:"id"`
	Project   *response.RelationshipData `json:"project"`
	URL       string                     `json:"url"`
	CreatedAt time.Time                  `json:"created_at"`
	UpdatedAt time.Time                  `json:"updated_at"`
}

func sanitizePhoto(p *adioo.Photo, e adioo.EncryptionService) (*sanitizedPhoto, error) {
	fullURL, err := e.EncryptURL(p.URL)
	if err != nil {
		return nil, err
	}

	return &sanitizedPhoto{
		ID: p.ID,
		Project: &response.RelationshipData{
			ID:   p.ProjectID,
			HREF: fmt.Sprintf("/projects/%s", p.ID),
		},
		URL:       fullURL,
		CreatedAt: p.CreatedAt,
		UpdatedAt: p.UpdatedAt,
	}, nil
}

func sanitizePhotos(s []*adioo.Photo, e adioo.EncryptionService) ([]*sanitizedPhoto, error) {
	ss := make([]*sanitizedPhoto, len(s))
	for x, u := range s {
		nu, err := sanitizePhoto(u, e)
		if err != nil {
			return nil, err
		}
		ss[x] = nu
	}

	return ss, nil
}
