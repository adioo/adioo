package httpphoto

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo/mock"
)

func TestCreatePhotoRequest_Validate(t *testing.T) {
	tests := []struct {
		req      *createPhotoRequest
		expected bool
	}{
		{&createPhotoRequest{ProjectID: mock.TestID, URL: mock.TestURL}, true},
		{&createPhotoRequest{ProjectID: mock.TestID, URL: mock.TestURL, ThumbnailURL: "invalid"}, false},
		{&createPhotoRequest{ProjectID: mock.TestID, URL: "invalidurl"}, false},
		{&createPhotoRequest{ProjectID: "invaliduuid", URL: mock.TestURL}, false},
		{&createPhotoRequest{ProjectID: mock.TestID}, false},
		{&createPhotoRequest{URL: mock.TestURL}, false},
	}

	for _, test := range tests {
		err := test.req.Validate()
		result := err == nil

		assert.Equal(t, test.expected, result)
	}
}
