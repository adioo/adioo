package http

// touch handler.go handler_test.go request.go request_test.go response.go main_test.go transport.go transport_test.go

import (
	"errors"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/adioo/adioo/http/httpauth"
	"gitlab.com/adioo/adioo/http/httpcompany"
	"gitlab.com/adioo/adioo/http/httpcustomer"
	"gitlab.com/adioo/adioo/http/httpemployee"
	"gitlab.com/adioo/adioo/http/httpevent"
	"gitlab.com/adioo/adioo/http/httpfolder"
	"gitlab.com/adioo/adioo/http/httpgallery"
	"gitlab.com/adioo/adioo/http/httpphoto"
	"gitlab.com/adioo/adioo/http/httpproject"
	"gitlab.com/adioo/adioo/http/middleware"
	"gitlab.com/adioo/adioo/http/response"
)

// HandlerConfig is
type HandlerConfig struct {
	CompanyHandler  *httpcompany.Handler
	CustomerHandler *httpcustomer.Handler
	AuthHandler     *httpauth.Handler
	FolderHandler   *httpfolder.Handler
	GalleryHandler  *httpgallery.Handler
	EventHandler    *httpevent.Handler
	PhotoHandler    *httpphoto.Handler
	ProjectHandler  *httpproject.Handler
	EmployeeHandler *httpemployee.Handler
}

func (c *HandlerConfig) validate() error {
	switch {
	case c.CompanyHandler == nil:
		return errors.New("company handler is required")
	case c.CustomerHandler == nil:
		return errors.New("customer handler is required")
	case c.AuthHandler == nil:
		return errors.New("auth handler is required")
	case c.FolderHandler == nil:
		return errors.New("folder handler is required")
	case c.GalleryHandler == nil:
		return errors.New("gallery handler is required")
	case c.EventHandler == nil:
		return errors.New("event handler is required")
	case c.PhotoHandler == nil:
		return errors.New("photo handler is required")
	case c.ProjectHandler == nil:
		return errors.New("project handler is required")
	case c.EmployeeHandler == nil:
		return errors.New("employee handler is required")
	}
	return nil
}

// Handler is
type Handler struct {
	*mux.Router
	CompanyHandler  *httpcompany.Handler
	CustomerHandler *httpcustomer.Handler
	AuthHandler     *httpauth.Handler
	FolderHandler   *httpfolder.Handler
	GalleryHandler  *httpgallery.Handler
	EventHandler    *httpevent.Handler
	PhotoHandler    *httpphoto.Handler
	ProjectHandler  *httpproject.Handler
	EmployeeHandler *httpemployee.Handler
}

// NewHandler is
func NewHandler(c *HandlerConfig) (*Handler, error) {
	if err := c.validate(); err != nil {
		return nil, err
	}

	// TODO error handling to make sure all services are passed in
	h := &Handler{
		Router:          mux.NewRouter(),
		CompanyHandler:  c.CompanyHandler,
		CustomerHandler: c.CustomerHandler,
		AuthHandler:     c.AuthHandler,
		FolderHandler:   c.FolderHandler,
		GalleryHandler:  c.GalleryHandler,
		EventHandler:    c.EventHandler,
		PhotoHandler:    c.PhotoHandler,
		ProjectHandler:  c.ProjectHandler,
		EmployeeHandler: c.EmployeeHandler,
	}

	authMiddleware := &middleware.Auth{
		BlacklistService: c.AuthHandler.BlacklistService,
		EmployeeStorage:  c.EmployeeHandler.EmployeeStorage,
	}

	h.routes(authMiddleware)

	return h, nil
}

func (h *Handler) handleHealth() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		resp := &response.Message{
			Data: &response.MessageData{
				Message: "server is healthy",
			},
		}
		response.OK(w, resp)
	})
}

// redirectTLS is
func redirectTLS(w http.ResponseWriter, r *http.Request) {
	target := "https://" + r.Host + r.URL.Path
	if len(r.URL.RawQuery) > 0 {
		target += "?" + r.URL.RawQuery
	}
	log.Printf("redirect to: %s", target)
	http.Redirect(w, r, target, http.StatusTemporaryRedirect)
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.Router.ServeHTTP(w, r)
}
