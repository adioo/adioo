package response

import (
	"encoding/json"
	"net/http"
	"os"

	"gitlab.com/adioo/adioo"
)

type PaginationData struct {
	Links   LinksData `json:"links"`
	Count   int       `json:"count"`
	SinceID string    `json:"since_id"`
}

type LinksData struct {
	Self string `json:"self"`
	Next string `json:"next"`
}

type RelationshipData struct {
	ID   string `json:"id"`
	HREF string `json:"href"`
}

type RelationshipsData struct {
	HREF string `json:"href"`
}

// Error is a generic HTTP response body for errors.
type Error struct {
	Error     string `json:"error,omitempty"`
	Type      string `json:"type"`
	RequestID string `json:"request_id"`
}

type Message struct {
	Data *MessageData `json:"data"`
}

type MessageData struct {
	Message string `json:"message"`
}

// EncryptURL is

// Error writes an API error message to the response and logger.
func WithError(w http.ResponseWriter, err error, code int, reqID string) {
	if err == adioo.ErrNotFound {
		code = http.StatusNotFound
	}

	if err == adioo.ErrProjectNameTaken || err == adioo.ErrFolderNameTaken {
		code = http.StatusBadRequest
	}

	// Hide error from client if it is internal.
	if code == http.StatusInternalServerError && os.Getenv("DEBUG") != "true" {
		err = adioo.ErrInternal
	}

	var errType string
	switch code {
	case http.StatusInternalServerError:
		errType = "Internal error"
		break
	case http.StatusBadRequest:
		errType = "Validation error"
		break
	case http.StatusNotFound:
		errType = "Not Found"
		break
	case http.StatusForbidden:
		errType = "Authentication error"
		break
	default:
		errType = "Internal error"
	}

	// Write generic error response.
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	resp := &Error{Error: err.Error(), Type: errType, RequestID: reqID}
	encodeJSON(w, resp)
}

// NotFound writes an API error message to the response.
func NotFound(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(`{}` + "\n"))
}

// OK encodes a JSON response and sets the status to HTTP Status to 200 OK
func OK(w http.ResponseWriter, v interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	encodeJSON(w, v)
}

// Created encodes a JSON response and sets the status to HTTP Status to 201 CREATED
func Created(w http.ResponseWriter, v interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	encodeJSON(w, v)
}

// encodeJSON encodes v to w in JSON format. Error() is called if encoding fails.
func encodeJSON(w http.ResponseWriter, v interface{}) {
	if err := json.NewEncoder(w).Encode(v); err != nil {
		WithError(w, err, http.StatusInternalServerError, "")
	}
}
