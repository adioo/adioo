package adioo

// AuthService is
type AuthService interface {
	Token(employeeID, companyID string) (string, error)
}

// BlacklistStorage is
type BlacklistStorage interface {
	Create(b *Blacklist) error
	CheckJTI(jti string) (bool, error)
}

// Blacklist is
type Blacklist struct {
	JTI     string
	Expires int64
}
