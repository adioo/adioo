package mock

import (
	"time"

	"gitlab.com/adioo/adioo"
)

// TestCompanyName is
const TestCompanyName = "test company name"

// CompanyService is
type CompanyService struct {
}

// Create is
func (s *CompanyService) Create(n *adioo.NewCompany) (*adioo.Company, error) {
	if n.Name != TestCompanyName {
		return nil, adioo.ErrInternal
	}

	c := &adioo.Company{
		Name:        n.Name,
		PhoneNumber: n.PhoneNumber,
		Email:       n.Email,
		Address:     n.Address,
		State:       n.State,
		ZipCode:     n.ZipCode,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	return c, nil
}

// Update is
func (s *CompanyService) Update(u *adioo.UpdateCompany) (*adioo.Company, error) {
	c, err := s.Company(u.ID)
	if err != nil {
		return nil, err
	}

	c.Name = u.Name
	c.PhoneNumber = u.PhoneNumber
	c.Email = u.Email
	c.Address = u.Address
	c.State = u.State
	c.ZipCode = u.ZipCode
	c.UpdatedAt = time.Now()

	return c, nil
}

// Company is
func (s *CompanyService) Company(id string) (*adioo.Company, error) {
	if id != TestID {
		return nil, adioo.ErrNotFound
	}
	c := new(adioo.Company)
	return c, nil
}
