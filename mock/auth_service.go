package mock

import (
	"gitlab.com/adioo/adioo"
)

// AuthService is
type AuthService struct{}

// Token is
func (s *AuthService) Token(employeeID, companyID string) (string, error) {
	if employeeID != TestID {
		return "", adioo.ErrInternal
	}
	return "testtoken", nil
}
