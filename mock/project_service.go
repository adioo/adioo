package mock

import (
	"strings"
	"time"

	"gitlab.com/adioo/adioo"
)

// ProjectService is
type ProjectService struct {
}

// Create is
func (s *ProjectService) Create(n *adioo.NewProject) (*adioo.Project, error) {
	p := &adioo.Project{
		CompanyID:    n.CompanyID,
		Name:         n.Name,
		Description:  n.Description,
		CustomerID:   n.CustomerID,
		CategoryID:   n.CategoryID,
		Location:     n.Location,
		ThumbnailURL: n.ThumbnailURL,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	return p, nil
}

// Project is
func (s *ProjectService) Project(id string) (*adioo.Project, error) {
	if id != TestID {
		return nil, adioo.ErrNotFound
	}
	p := new(adioo.Project)

	return p, nil
}

// Update is
func (s *ProjectService) Update(u *adioo.UpdateProject) (*adioo.Project, error) {
	p, err := s.Project(u.ID)
	if err != nil {
		return nil, err
	}

	p.Name = u.Name
	p.CategoryID = u.CategoryID
	p.CustomerID = u.CustomerID
	p.Description = u.Description
	p.UpdatedAt = time.Now()
	p.Location = u.Location
	p.ThumbnailURL = u.ThumbnailURL

	return p, nil
}

// ListCompanyProjects is
func (s *ProjectService) ListCompanyProjects(companyID, categoryID string) ([]*adioo.Project, error) {
	if companyID != TestID {
		return nil, adioo.ErrNotFound
	}
	var p []*adioo.Project
	return p, nil
}

// ListProjectPhotos is
func (s *ProjectService) ListProjectPhotos(projectID string) ([]*adioo.Photo, error) {
	var p []*adioo.Photo

	return p, nil
}

// ListCategoryProjects is
func (s *ProjectService) ListCategoryProjects(categoryID string) ([]*adioo.Project, error) {
	var p []*adioo.Project

	return p, nil
}

// CreateCategory is
func (s *ProjectService) CreateCategory(companyID, name, description string) (*adioo.ProjectCategory, error) {
	c := &adioo.ProjectCategory{
		Name:        strings.ToLower(name),
		CompanyID:   companyID,
		Description: description,
		CreatedAt:   time.Now(),
	}

	return c, nil
}

// Category is
func (s *ProjectService) Category(id string) (*adioo.ProjectCategory, error) {
	c := new(adioo.ProjectCategory)

	return c, nil
}

// CompanyCategory is
func (s *ProjectService) CompanyCategory(companyID, name string) (*adioo.ProjectCategory, error) {
	c := new(adioo.ProjectCategory)

	return c, nil
}

// ListCompanyCategories is
func (s *ProjectService) ListCompanyCategories(companyID string) ([]*adioo.ProjectCategory, error) {
	if companyID != TestID {
		return nil, adioo.ErrNotFound
	}
	var c []*adioo.ProjectCategory

	return c, nil
}

// AddPhoto is
func (s *ProjectService) AddPhoto(projectID, url, turl string) (*adioo.Photo, error) {
	p := &adioo.Photo{
		ProjectID:    projectID,
		URL:          url,
		ThumbnailURL: turl,
		CreatedAt:    time.Now(),
	}

	return p, nil
}

// RemovePhoto is
func (s *ProjectService) RemovePhoto(id string) error {

	return nil
}
