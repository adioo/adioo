package mock

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/adioo/adioo"
)

// GalleryService is
type GalleryService struct {
}

// Create is
func (s *GalleryService) Create(title, employeeID string, photoIDs []string, permissions []string) (*adioo.Gallery, error) {
	return new(adioo.Gallery), nil
}

// Gallery is
func (s *GalleryService) Gallery(id string) (*adioo.Gallery, error) {
	if id != TestID {
		return nil, adioo.ErrNotFound
	}
	l := new(adioo.Gallery)
	return l, nil
}

// AddPhoto is
func (s *GalleryService) AddPhoto(galleryID, src string) (*adioo.GalleryPhoto, error) {
	p := &adioo.GalleryPhoto{
		ID:         uuid.NewV4().String(),
		Src:        src,
		GalleryID:  galleryID,
		IsOriginal: true,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	return p, nil
}

// ListEmployeeGalleries is
func (s *GalleryService) ListEmployeeGalleries(employeeID string) ([]*adioo.Gallery, error) {
	if employeeID != TestID {
		return nil, adioo.ErrNotFound
	}
	var g []*adioo.Gallery
	return g, nil
}

// ListCompanyGalleries is
func (s *GalleryService) ListCompanyGalleries(companyID string) ([]*adioo.Gallery, error) {
	if companyID != TestID {
		return nil, adioo.ErrNotFound
	}
	var g []*adioo.Gallery
	return g, nil
}

// RemovePhoto is
func (s *GalleryService) RemovePhoto(id string) error {
	if id != TestID {
		return adioo.ErrNotFound
	}
	return nil
}

// ListPermissions is
func (s *GalleryService) ListPermissions(names []string) ([]*adioo.Permission, error) {
	var p []*adioo.Permission
	return p, nil
}

// ListPhotoComments is
func (s *GalleryService) ListPhotoComments(photoID string) ([]*adioo.GalleryPhotoComment, error) {
	if photoID != TestID {
		return nil, adioo.ErrNotFound
	}
	c := []*adioo.GalleryPhotoComment{
		new(adioo.GalleryPhotoComment),
	}
	return c, nil
}

// GetPhoto is
func (s *GalleryService) GetPhoto(photoID string) (*adioo.GalleryPhoto, error) {
	if photoID != TestID {
		return nil, adioo.ErrNotFound
	}
	p := new(adioo.GalleryPhoto)
	return p, nil
}

// CreatePhotoComment is
func (s *GalleryService) CreatePhotoComment(n *adioo.NewGalleryPhotoComment) (*adioo.GalleryPhotoComment, error) {
	c := &adioo.GalleryPhotoComment{
		ID:         uuid.NewV4().String(),
		PhotoID:    n.PhotoID,
		Text:       n.Text,
		Anchor:     n.Anchor,
		CustomerID: n.CustomerID,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	return c, nil

}
