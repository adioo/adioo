package mock

import (
	"time"

	"gitlab.com/adioo/adioo"
)

// EventService is
type EventService struct {
}

// Create is
func (s *EventService) Create(n *adioo.NewEvent) (*adioo.Event, error) {
	e := &adioo.Event{
		EmployeeID:  n.EmployeeID,
		CustomerID:  n.CustomerID,
		Title:       n.Title,
		Description: n.Description,
		Location:    n.Location,
		Notes:       n.Notes,
		CreatedAt:   time.Now(),
	}

	return e, nil
}

// Event is
func (s *EventService) Event(id string) (*adioo.Event, error) {
	e := new(adioo.Event)

	return e, nil
}

// ListEmployeeEvents is
func (s *EventService) ListEmployeeEvents(employeeID, start, end, sinceID string, limit int) ([]*adioo.Event, error) {
	var e []*adioo.Event

	return e, nil
}

// Count is
func (s *EventService) Count() (int, error) {
	return 1, nil
}
