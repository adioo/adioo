package mock

import (
	"time"

	"gitlab.com/adioo/adioo"
)

// FolderService is
type FolderService struct {
}

// Create is
func (s *FolderService) Create(n *adioo.NewFolder) (*adioo.Folder, error) {
	f := &adioo.Folder{
		ProjectID:   n.ProjectID,
		Name:        n.Name,
		Description: n.Description,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	return f, nil
}

// Folder is
func (s *FolderService) Folder(id string) (*adioo.Folder, error) {
	if id != TestID {
		return nil, adioo.ErrNotFound
	}

	f := new(adioo.Folder)
	f.Photos = []*adioo.Photo{
		new(adioo.Photo),
	}

	return f, nil
}

// ListProjectFolders is
func (s *FolderService) ListProjectFolders(projectID string) ([]*adioo.Folder, error) {
	var f []*adioo.Folder
	return f, nil
}

// AddPhoto is
func (s *FolderService) AddPhoto(folderID, PhotoID string) error {

	return nil
}

// RemovePhoto is
func (s *FolderService) RemovePhoto(folderID, PhotoID string) error {

	return nil
}
