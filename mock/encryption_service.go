package mock

// EncryptionService is
type EncryptionService struct {
}

// EncryptURL is
func (s *EncryptionService) EncryptURL(u string) (string, error) {
	return "encrypted", nil
}

// DecryptURL is
func (s *EncryptionService) DecryptURL(u string) (string, error) {
	return "dencrypted", nil
}

// DecryptURLs is
func (s *EncryptionService) DecryptURLs(us []string) ([]string, error) {
	return []string{}, nil
}
