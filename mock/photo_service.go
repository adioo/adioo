package mock

import (
	"time"

	"gitlab.com/adioo/adioo"
)

// PhotoService is
type PhotoService struct {
}

// Create is
func (s *PhotoService) Create(n *adioo.NewPhoto) (*adioo.Photo, error) {
	p := &adioo.Photo{
		ProjectID:    n.ProjectID,
		URL:          n.URL,
		ThumbnailURL: n.ThumbnailURL,
		CreatedAt:    time.Now(),
	}

	return p, nil
}

// AddFolderPhoto is
func (s *PhotoService) AddFolderPhoto(folderID, PhotoID string) error {
	return nil
}

// ListProjectPhotos is
func (s *PhotoService) ListProjectPhotos(projectID string) ([]*adioo.Photo, error) {
	p := []*adioo.Photo{
		new(adioo.Photo),
	}

	return p, nil
}

// RemovePhoto is
func (s *PhotoService) RemovePhoto(id string) error {
	return nil
}

// Photo is
func (s *PhotoService) Photo(id string) (*adioo.Photo, error) {
	p := new(adioo.Photo)
	return p, nil
}

// RemoveFolderPhoto is
func (s *PhotoService) RemoveFolderPhoto(folderID, PhotoID string) error {
	return nil
}
