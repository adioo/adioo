package mock

import (
	"time"

	"gitlab.com/adioo/adioo"
)

//TestCustomerName is
const TestCustomerName = "test customer name"

// CustomerService is
type CustomerService struct {
}

// Create is
func (s *CustomerService) Create(n *adioo.NewCustomer) (*adioo.Customer, error) {
	if n.FirstName != TestCustomerName {
		return nil, adioo.ErrInternal
	}

	c := &adioo.Customer{
		CompanyID:   n.CompanyID,
		FirstName:   n.FirstName,
		LastName:    n.LastName,
		PhoneNumber: n.PhoneNumber,
		Email:       n.Email,
		CreatedAt:   time.Now(),
	}

	return c, nil
}

// Customer is
func (s *CustomerService) Customer(id string) (*adioo.Customer, error) {
	c := new(adioo.Customer)
	if id != TestID {
		return nil, adioo.ErrNotFound
	}
	return c, nil
}

// ListCompanyCustomers is
func (s *CustomerService) ListCompanyCustomers(companyID, sinceID string, limit int) ([]*adioo.Customer, error) {
	if companyID != TestID {
		return nil, adioo.ErrNotFound
	}

	var c []*adioo.Customer
	customer := new(adioo.Customer)
	c = append(c, customer)
	return c, nil
}
