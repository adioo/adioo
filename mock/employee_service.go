package mock

import (
	"time"

	"github.com/satori/go.uuid"

	"gitlab.com/adioo/adioo"
)

// TestJTI is
const TestJTI = "adc3eee3919f0ea00b32081f69e7c769"

// EmployeeService is
type EmployeeService struct {
}

// Create is
func (s *EmployeeService) Create(n *adioo.NewEmployee) (*adioo.Employee, error) {
	e := &adioo.Employee{
		ID:          uuid.NewV4().String(),
		CompanyID:   n.CompanyID,
		FirstName:   n.FirstName,
		LastName:    n.LastName,
		Email:       n.Email,
		PhoneNumber: n.PhoneNumber,
		AvatarURL:   n.AvatarURL,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	return e, nil
}

// ListCompanyEmployees is
func (s *EmployeeService) ListCompanyEmployees(companyID, sinceID string, limit int) ([]*adioo.Employee, error) {
	if companyID != TestID {
		return nil, adioo.ErrNotFound
	}
	var e []*adioo.Employee

	return e, nil
}

// Employee is
func (s *EmployeeService) Employee(id string) (*adioo.Employee, error) {
	e := new(adioo.Employee)
	if id == WrongTestID {
		return nil, adioo.ErrNotFound
	}
	e.JTI = TestJTI
	e.ID = TestID
	return e, nil
}

// Update is
func (s *EmployeeService) Update(u *adioo.UpdateEmployee) (*adioo.Employee, error) {
	e, err := s.Employee(u.ID)
	if err != nil {
		return nil, err
	}

	e.FirstName = u.FirstName
	e.LastName = u.LastName
	e.Email = u.Email
	e.PhoneNumber = u.PhoneNumber
	e.AvatarURL = u.AvatarURL
	e.UpdatedAt = time.Now()

	return e, nil
}

// Authenticate is
func (s *EmployeeService) Authenticate(email, password string) (*adioo.Employee, error) {
	e := new(adioo.Employee)
	if email != TestEmail {
		return nil, adioo.ErrNotFound
	}
	e.ID = TestID
	return e, nil
}

// EmployeePermissions is
func (s *EmployeeService) EmployeePermissions(id string) ([]*adioo.Permission, error) {
	var p []*adioo.Permission

	return p, nil
}

// ListPermissions is
func (s *EmployeeService) ListPermissions(names []string) ([]*adioo.Permission, error) {
	var p []*adioo.Permission

	return p, nil
}

// UpdateJTI is
func (s *EmployeeService) UpdateJTI(employeeID, jti string) error {
	return nil
}

// GetJTI is
func (s *EmployeeService) GetJTI(jti string) (string, error) {
	e := new(adioo.Employee)

	return e.JTI, nil
}
