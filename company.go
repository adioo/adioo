package adioo

import "time"

// CompanyService is
type CompanyService interface {
	Create(n *NewCompany) (*Company, error)
	Update(n *UpdateCompany) (*Company, error)
	Company(id string) (*Company, error)
	// CompanyWith(relationships ...string) (*Company, error)
}

// Company is
type Company struct {
	ID          string      `json:"id"`
	Name        string      `json:"string"`
	PhoneNumber string      `json:"phone_number"`
	Email       string      `json:"email"`
	Address     string      `json:"address"`
	State       string      `json:"state"`
	ZipCode     int         `json:"zip_code"`
	Employees   []*Employee `json:"employees"`
	Customers   []*Customer `json:"customers"`
	CreatedAt   time.Time   `json:"created_at"`
	UpdatedAt   time.Time   `json:"updated_at"`
}

// UpdateCompany is
type UpdateCompany struct {
	ID          string
	Name        string
	PhoneNumber string
	Email       string
	Address     string
	State       string
	ZipCode     int
}

// NewCompany is
type NewCompany struct {
	Name        string
	PhoneNumber string
	Email       string
	Address     string
	State       string
	ZipCode     int
}
