package adioo

import "time"

// EventService is
type EventService interface {
	Create(n *NewEvent) (*Event, error)
	ListEmployeeEvents(employeeID, start, end, sinceID string, limit int) ([]*Event, error)
	Count() (int, error)
}

// Event is
type Event struct {
	ID          string    `json:"id"`
	EmployeeID  string    `json:"employee_id"`
	CustomerID  string    `json:"customer_id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Start       time.Time `json:"start"`
	End         time.Time `json:"end"`
	Location    string    `json:"location"`
	Notes       string    `json:"notes"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// NewEvent is
type NewEvent struct {
	EmployeeID  string `json:"employee_id"`
	CustomerID  string `json:"customer_id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Start       string `json:"start"`
	End         string `json:"end"`
	Location    string `json:"location"`
	Notes       string `json:"notes"`
}
