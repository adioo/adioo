package postgre

import (
	"fmt"
	"strings"
	"time"

	"github.com/satori/go.uuid"

	"golang.org/x/crypto/bcrypt"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

// EmployeeService is
type EmployeeService struct {
	DB *pg.DB
}

// Create is
func (s *EmployeeService) Create(n *adioo.NewEmployee) (*adioo.Employee, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(n.Password), 10)
	if err != nil {
		return nil, err
	}

	e := &adioo.Employee{
		ID:          uuid.NewV4().String(),
		CompanyID:   n.CompanyID,
		FirstName:   n.FirstName,
		LastName:    n.LastName,
		Email:       n.Email,
		PhoneNumber: n.PhoneNumber,
		Password:    string(b),
		AvatarURL:   n.AvatarURL,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	if len(n.Permissions) == 0 {
		if err := s.DB.Insert(e); err != nil {
			return nil, err
		}

		return e, nil
	}

	values := []interface{}{
		nullString(e.ID),
		nullString(e.CompanyID),
		nullString(e.FirstName),
		nullString(e.LastName),
		nullString(e.Email),
		nullString(e.PhoneNumber),
		nullString(e.Password),
		e.CreatedAt,
		e.UpdatedAt,
	}

	permissions, err := s.ListPermissions(n.Permissions)
	if err != nil {
		return nil, err
	}

	fmt.Println("perm found", len(permissions))

	e.Permissions = permissions

	q := `
		WITH employee as (INSERT INTO employees(id, company_id, first_name, last_name, email, phone_number, password, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *) INSERT INTO employee_permission(employee_id, permission_id) VALUES
	`

	for _, p := range permissions {
		q += "((select employee.id from employee), ?),"
		values = append(values, nullString(p.ID))
	}

	q = strings.TrimSuffix(q, ",")
	if _, err := s.DB.Query(e, q, values...); err != nil {
		return nil, err
	}

	return e, nil
}

// ListCompanyEmployees is
func (s *EmployeeService) ListCompanyEmployees(companyID, sinceID string, limit int) ([]*adioo.Employee, error) {
	var e []*adioo.Employee
	if sinceID != "" {
		sinceEmployee, err := s.Employee(sinceID)
		if err != nil {
			return nil, err
		}
		if err := s.DB.Model(&e).Where("company_id = ? and created_at < ?", companyID, sinceEmployee.CreatedAt).Limit(limit).Order("created_at DESC").Select(); err != nil {
			return nil, err
		}
		return e, nil
	}
	if err := s.DB.Model(&e).Column("*", "Permissions").Where("company_id = ?", companyID).Order("created_at DESC").Limit(limit).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return e, nil
}

// Employee is
func (s *EmployeeService) Employee(id string) (*adioo.Employee, error) {
	e := new(adioo.Employee)
	if err := s.DB.Model(&e).Column("*", "Permissions").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return e, nil
}

// Update is
func (s *EmployeeService) Update(u *adioo.UpdateEmployee) (*adioo.Employee, error) {
	e, err := s.Employee(u.ID)
	if err != nil {
		return nil, err
	}

	e.FirstName = u.FirstName
	e.LastName = u.LastName
	e.Email = u.Email
	e.PhoneNumber = u.PhoneNumber
	e.AvatarURL = u.AvatarURL
	e.UpdatedAt = time.Now()

	if _, err := s.DB.Model(e).Column("first_name", "last_name", "email", "phone_number", "avatar_url", "updated_at").Update(e); err != nil {
		return nil, err
	}

	return e, nil
}

// Authenticate is
func (s *EmployeeService) Authenticate(email, password string) (*adioo.Employee, error) {
	e := new(adioo.Employee)
	if err := s.DB.Model(e).Where("email = ?", email).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(e.Password), []byte(password)); err != nil {
		return nil, adioo.ErrNotFound
	}

	return e, nil
}

// EmployeePermissions is
func (s *EmployeeService) EmployeePermissions(id string) ([]*adioo.Permission, error) {
	var p []*adioo.Permission
	if err := s.DB.Model(&p).Column("permission.*", "Employees.id").Join("inner join employee_permission ep on permission.id = ep.permission_id").Where("ep.employee_id = ?", id).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}

	return p, nil
}

// ListPermissions is
func (s *EmployeeService) ListPermissions(names []string) ([]*adioo.Permission, error) {
	var inames []interface{}
	for _, n := range names {
		inames = append(inames, n)
	}

	var p []*adioo.Permission
	if err := s.DB.Model(&p).WhereIn("name IN (?)", inames...).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return p, nil
}

// UpdateJTI is
func (s *EmployeeService) UpdateJTI(employeeID, jti string) error {
	e, err := s.Employee(employeeID)
	if err != nil {
		return err
	}
	e.JTI = jti
	if _, err := s.DB.Model(e).Column("jti", "updated_at").Update(e); err != nil {
		return err
	}
	return nil
}

// GetJTI is
func (s *EmployeeService) GetJTI(jti string) (string, error) {
	e := new(adioo.Employee)
	if err := s.DB.Model(e).Where("jti = ?", jti).First(); err != nil {
		if err == pg.ErrNoRows {
			return "", adioo.ErrNotFound
		}
		return "", err
	}
	return e.JTI, nil
}
