package postgre

import (
	"testing"

	"github.com/icrowley/fake"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adioo/adioo"
)

func TestCreateCompany(t *testing.T) {
	tests := []struct {
		n        *adioo.NewCompany
		expected bool
	}{
		{&adioo.NewCompany{Name: fake.ProductName(), PhoneNumber: fake.Phone(), Email: fake.EmailAddress(), Address: "21323", State: "DL", ZipCode: 57677}, true},
		{&adioo.NewCompany{Name: fake.ProductName(), PhoneNumber: fake.Phone(), Address: "21323", State: "DL", ZipCode: 57677}, true},
		{&adioo.NewCompany{Name: fake.ProductName(), Address: "21323", State: "DL", ZipCode: 57677}, false},
		{&adioo.NewCompany{Name: fake.ProductName(), PhoneNumber: fake.Phone(), Email: fake.EmailAddress(), Address: "21323", State: "DL", ZipCode: 57677}, true},
		{&adioo.NewCompany{Name: fake.ProductName(), Email: fake.EmailAddress(), State: "DL", ZipCode: 57677}, true},
		{&adioo.NewCompany{Name: fake.ProductName(), PhoneNumber: fake.Phone(), Email: fake.EmailAddress(), Address: "21323", ZipCode: 57677}, true},
		{&adioo.NewCompany{Name: fake.ProductName(), PhoneNumber: fake.Phone(), Email: fake.EmailAddress(), Address: "21323", State: "DL"}, true},
		{&adioo.NewCompany{PhoneNumber: fake.Phone(), Email: fake.EmailAddress(), Address: "21323", State: "DL"}, false},
	}

	for i, test := range tests {
		c, err := testCompanyService.Create(test.n)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting result to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}

		if err == nil {
			assert.NotNil(t, c.ID)
		}
	}
}
