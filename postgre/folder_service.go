package postgre

import (
	"time"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

const (
	pgUniqueFolderNameErr = "unique_project_id_name"
)

// FolderService is
type FolderService struct {
	DB *pg.DB
}

// Create is
func (s *FolderService) Create(n *adioo.NewFolder) (*adioo.Folder, error) {
	f := &adioo.Folder{
		ProjectID:   n.ProjectID,
		Name:        n.Name,
		Description: n.Description,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	if err := s.DB.Insert(f); err != nil {
		if pgErr, ok := err.(pg.Error); ok {
			if pgErr.Field('n') == pgUniqueFolderNameErr {
				return nil, adioo.ErrFolderNameTaken
			}
		}
		return nil, err
	}

	return f, nil
}

// Folder is
func (s *FolderService) Folder(id string) (*adioo.Folder, error) {
	f := new(adioo.Folder)
	if err := s.DB.Model(&f).Column("*", "Photos").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return f, nil
}

// ListProjectFolders is
func (s *FolderService) ListProjectFolders(projectID string) ([]*adioo.Folder, error) {
	var f []*adioo.Folder
	if err := s.DB.Model(&f).Column("*", "Photos").Where("project_id = ?", projectID).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return f, nil
}
