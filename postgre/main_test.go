package postgre

import (
	"fmt"
	"log"
	"os"
	"testing"

	"gitlab.com/adioo/adioo"

	"github.com/joho/godotenv"
	pg "gopkg.in/pg.v5"
)

var (
	testCompanyService adioo.CompanyService
)

func TestMain(m *testing.M) {
	if err := godotenv.Load("/Users/digo/code/src/gitlab.com/adioo/adioo/cmd/api/.env"); err != nil {
		log.Fatal("Error loading .env file")
	}

	db := pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USER"),
		Addr:     os.Getenv("DB_ADDR"),
		Database: os.Getenv("DB_NAME"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	defer db.Close()

	cleanup(db)
	setup(db)

	t := m.Run()

	os.Exit(t)
}

func setup(db *pg.DB) {
	testCompanyService = &CompanyService{
		DB: db,
	}
}

func cleanup(db *pg.DB) {
	if _, err := db.Query(adioo.Company{}, "TRUNCATE companies, customers, events, employees CASCADE"); err != nil {
		fmt.Println("error on db cleanup", err)
	}
}
