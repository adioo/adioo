package postgre

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/satori/go.uuid"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

// GalleryService is
type GalleryService struct {
	DB              *pg.DB
	EmployeeStorage adioo.EmployeeService
}

// Create is
func (s *GalleryService) Create(title, employeeID string, photos []string, permissions []string) (*adioo.Gallery, error) {
	g := &adioo.Gallery{
		ID:         uuid.NewV4().String(),
		Title:      title,
		EmployeeID: employeeID,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	values := []interface{}{
		nullString(g.ID),
		nullString(g.Title),
		nullString(g.EmployeeID),
		g.CreatedAt,
		g.UpdatedAt,
	}

	q := `
		WITH gallery as (INSERT INTO galleries(id, title, employee_id, created_at, updated_at) VALUES (?, ?, ?, ?, ?) RETURNING *) INSERT INTO gallery_photos(gallery_id, src, is_original, created_at) VALUES
	`

	for _, photo := range photos {
		q += "((select gallery.id from gallery), ?, ?, ?),"
		values = append(values, photo, true, time.Now())
	}

	q = strings.TrimSuffix(q, ",")
	if _, err := s.DB.Query(g, q, values...); err != nil {
		return nil, err
	}

	if len(permissions) > 0 {
		p, err := s.ListPermissions(permissions)
		if err != nil {
			return nil, err
		}
		g.Permissions = p
		for _, per := range p {
			lp := &adioo.GalleryPermission{
				GalleryID:    g.ID,
				PermissionID: per.ID,
			}
			if err := s.DB.Insert(lp); err != nil {
				return nil, err
			}
		}
	}

	go s.createRemainingUrls(g.ID)

	return g, nil
}

// ListEmployeeGalleries is
func (s *GalleryService) ListEmployeeGalleries(employeeID string) ([]*adioo.Gallery, error) {
	var g []*adioo.Gallery
	if err := s.DB.Model(&g).Column("*", "Permissions").Where("employee_id = ?", employeeID).Select(); err != nil {
		return nil, err
	}
	return g, nil
}

// ListCompanyGalleries is
func (s *GalleryService) ListCompanyGalleries(companyID string) ([]*adioo.Gallery, error) {
	var g []*adioo.Gallery
	employees, err := s.EmployeeStorage.ListCompanyEmployees(companyID, "", 50)
	if err != nil {
		return nil, err
	}
	var ids []interface{}
	for _, e := range employees {
		ids = append(ids, e.ID)
	}

	if err := s.DB.Model(&g).Column("*", "Permissions").WhereIn("employee_id IN (?)", ids...).Select(); err != nil {
		return nil, err
	}
	return g, nil
}

//Gallery is
func (s *GalleryService) Gallery(id string) (*adioo.Gallery, error) {
	g := new(adioo.Gallery)
	if err := s.DB.Model(g).Column("*", "Photos", "Permissions").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return g, nil
}

// AddPhoto is
func (s *GalleryService) AddPhoto(galleryID, src string) (*adioo.GalleryPhoto, error) {
	p := &adioo.GalleryPhoto{
		ID:         uuid.NewV4().String(),
		Src:        src,
		GalleryID:  galleryID,
		IsOriginal: true,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}
	if err := s.DB.Insert(p); err != nil {
		return nil, err
	}

	return p, nil
}

// RemovePhoto is
func (s *GalleryService) RemovePhoto(id string) error {
	r, err := s.DB.Model(&adioo.GalleryPhoto{}).Where("id = ?", id).Delete()
	if err != nil {
		return err
	}

	if r.RowsAffected() == 0 {
		return adioo.ErrNotFound
	}
	return nil
}

// ListPermissions is
func (s *GalleryService) ListPermissions(names []string) ([]*adioo.Permission, error) {
	var inames []interface{}
	for _, n := range names {
		inames = append(inames, n)
	}

	var p []*adioo.Permission
	if err := s.DB.Model(&p).WhereIn("name IN (?)", inames...).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return p, nil
}

// ListPhotoComments is
func (s *GalleryService) ListPhotoComments(photoID string) ([]*adioo.GalleryPhotoComment, error) {
	var c []*adioo.GalleryPhotoComment
	if err := s.DB.Model(&c).Where("photo_id = ?", photoID).Select(); err != nil {
		return nil, err
	}
	return c, nil
}

// GetPhoto is
func (s *GalleryService) GetPhoto(photoID string) (*adioo.GalleryPhoto, error) {
	p := new(adioo.GalleryPhoto)
	if err := s.DB.Model(&p).Where("id = ?", photoID).Select(); err != nil {
		return nil, err
	}
	return p, nil
}

// CreatePhotoComment is
func (s *GalleryService) CreatePhotoComment(n *adioo.NewGalleryPhotoComment) (*adioo.GalleryPhotoComment, error) {
	c := &adioo.GalleryPhotoComment{
		ID:         uuid.NewV4().String(),
		PhotoID:    n.PhotoID,
		Text:       n.Text,
		Anchor:     n.Anchor,
		CustomerID: n.CustomerID,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	if err := s.DB.Insert(c); err != nil {
		return nil, err
	}

	return c, nil

}

func (s *GalleryService) createRemainingUrls(galleryID string) {
	widths := []string{"640", "1024", "1920"}

	g, err := s.Gallery(galleryID)
	if err != nil {
		log.Println(err)
		return
	}

	var perms []string
	for _, per := range g.Permissions {
		perms = append(perms, per.Name)
	}

	for _, p := range g.Photos {
		url := fmt.Sprintf("%s?watermark=", p.Src)
		if !adioo.StringInSlice("downloadable", perms) {
			url += "Adioo"
		}
		for _, w := range widths {
			url += fmt.Sprintf("&width=%s", w)
			gp := &adioo.GalleryPhoto{
				Src:            url,
				GalleryID:      galleryID,
				IsOriginal:     false,
				GalleryPhotoID: p.ID,
				CreatedAt:      time.Now(),
			}
			if err := s.DB.Insert(gp); err != nil {
				log.Println(err)
				continue
			}
			url = fmt.Sprintf("%s?watermark=", p.Src)
		}
	}
}
