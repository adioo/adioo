package postgre

import (
	"time"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

// CustomerService is
type CustomerService struct {
	DB *pg.DB
}

// Create is
func (s *CustomerService) Create(n *adioo.NewCustomer) (*adioo.Customer, error) {
	c := &adioo.Customer{
		CompanyID:   n.CompanyID,
		FirstName:   n.FirstName,
		LastName:    n.LastName,
		PhoneNumber: n.PhoneNumber,
		Email:       n.Email,
		CreatedAt:   time.Now(),
	}

	if err := s.DB.Insert(c); err != nil {
		return nil, err
	}

	return c, nil
}

// Customer is
func (s *CustomerService) Customer(id string) (*adioo.Customer, error) {
	c := new(adioo.Customer)
	if err := s.DB.Model(&c).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return c, nil
}

// ListCompanyCustomers is
func (s *CustomerService) ListCompanyCustomers(companyID, sinceID string, limit int) ([]*adioo.Customer, error) {
	var c []*adioo.Customer
	if sinceID != "" {
		sinceCustomer, err := s.Customer(sinceID)
		if err != nil {
			return nil, err
		}
		if err := s.DB.Model(&c).Where("company_id = ? and created_at < ?", companyID, sinceCustomer.CreatedAt).Limit(limit).Order("created_at DESC").Select(); err != nil {
			return nil, err
		}
		return c, nil
	}
	if err := s.DB.Model(&c).Where("company_id = ?", companyID).Order("first_name").Order("created_at DESC").Limit(limit).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}

	return c, nil
}
