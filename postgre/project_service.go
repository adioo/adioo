package postgre

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

const (
	pgUniqueProjectNameErr = "unique_company_id_name"
)

// ProjectService is
type ProjectService struct {
	DB *pg.DB
}

// Create is
func (s *ProjectService) Create(n *adioo.NewProject) (*adioo.Project, error) {
	p := &adioo.Project{
		CompanyID:    n.CompanyID,
		Name:         n.Name,
		Description:  n.Description,
		CustomerID:   n.CustomerID,
		CategoryID:   n.CategoryID,
		Location:     n.Location,
		ThumbnailURL: n.ThumbnailURL,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	if err := s.DB.Insert(p); err != nil {
		if pgErr, ok := err.(pg.Error); ok {
			if pgErr.Field('n') == pgUniqueProjectNameErr {
				return nil, adioo.ErrProjectNameTaken
			}
		}
		return nil, err
	}

	return p, nil
}

// Project is
func (s *ProjectService) Project(id string) (*adioo.Project, error) {
	p := new(adioo.Project)
	if err := s.DB.Model(p).Column("*", "Photos").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return p, nil
}

// Update is
func (s *ProjectService) Update(u *adioo.UpdateProject) (*adioo.Project, error) {
	p, err := s.Project(u.ID)
	if err != nil {
		return nil, err
	}

	p.Name = u.Name
	p.CategoryID = u.CategoryID
	p.CustomerID = u.CustomerID
	p.Description = u.Description
	p.UpdatedAt = time.Now()
	p.Location = u.Location
	p.ThumbnailURL = u.ThumbnailURL

	if _, err := s.DB.Model(p).Column("name", "category_id", "customer_id", "updated_at", "location", "thumbnail_url").Update(); err != nil {
		return nil, err
	}

	return p, nil
}

// ListCompanyProjects is
func (s *ProjectService) ListCompanyProjects(companyID, categoryID string) ([]*adioo.Project, error) {
	if categoryID == "all" {
		var p []*adioo.Project
		if err := s.DB.Model(&p).Where("company_id = ?", companyID).Select(); err != nil {
			if err == pg.ErrNoRows {
				return nil, adioo.ErrNotFound
			}
			return nil, err
		}
		return p, nil
	}
	return s.ListCategoryProjects(categoryID)
}

// ListCategoryProjects is
func (s *ProjectService) ListCategoryProjects(categoryID string) ([]*adioo.Project, error) {
	var p []*adioo.Project
	if err := s.DB.Model(&p).Where("category_id = ?", categoryID).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return p, nil
}

// CreateCategory is
func (s *ProjectService) CreateCategory(companyID, name, description string) (*adioo.ProjectCategory, error) {
	c := &adioo.ProjectCategory{
		Name:        strings.ToLower(name),
		CompanyID:   companyID,
		Description: description,
		CreatedAt:   time.Now(),
	}
	fmt.Println(c.CompanyID)
	if err := s.DB.Insert(c); err != nil {
		return nil, err
	}
	return c, nil
}

// Category is
func (s *ProjectService) Category(id string) (*adioo.ProjectCategory, error) {
	c := new(adioo.ProjectCategory)
	if err := s.DB.Model(&c).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return c, nil
}

// CompanyCategory is
func (s *ProjectService) CompanyCategory(companyID, name string) (*adioo.ProjectCategory, error) {
	c := new(adioo.ProjectCategory)
	if err := s.DB.Model(&c).Where("company_id = ? and name = ?", companyID, name).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return c, nil
}

// ListCompanyCategories is
func (s *ProjectService) ListCompanyCategories(companyID string) ([]*adioo.ProjectCategory, error) {
	var c []*adioo.ProjectCategory
	if err := s.DB.Model(&c).Where("company_id = ?", companyID).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return c, nil
}
