package postgre

import (
	"time"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

// CompanyService is
type CompanyService struct {
	DB *pg.DB
}

// Create is
func (s *CompanyService) Create(n *adioo.NewCompany) (*adioo.Company, error) {
	c := &adioo.Company{
		Name:        n.Name,
		PhoneNumber: n.PhoneNumber,
		Email:       n.Email,
		Address:     n.Address,
		State:       n.State,
		ZipCode:     n.ZipCode,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	if err := s.DB.Insert(c); err != nil {
		return nil, err
	}

	return c, nil
}

// Update is
func (s *CompanyService) Update(u *adioo.UpdateCompany) (*adioo.Company, error) {
	c, err := s.Company(u.ID)
	if err != nil {
		return nil, err
	}

	c.Name = u.Name
	c.PhoneNumber = u.PhoneNumber
	c.Email = u.Email
	c.Address = u.Address
	c.State = u.State
	c.ZipCode = u.ZipCode
	c.UpdatedAt = time.Now()

	if _, err := s.DB.Model(c).Column("name", "phone_number", "email", "address", "state", "zip_code", "updated_at").Update(); err != nil {
		return nil, err
	}

	return c, nil
}

// Company is
func (s *CompanyService) Company(id string) (*adioo.Company, error) {
	c := new(adioo.Company)
	if err := s.DB.Model(c).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return c, nil
}
