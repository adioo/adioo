package postgre

import (
	"strconv"
	"time"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

// EventService is
type EventService struct {
	DB *pg.DB
}

// Create is
func (s *EventService) Create(n *adioo.NewEvent) (*adioo.Event, error) {
	intStart, err := strconv.ParseInt(n.Start, 10, 64)
	if err != nil {
		return nil, err
	}
	intEnd, err := strconv.ParseInt(n.End, 10, 64)
	if err != nil {
		return nil, err
	}
	e := &adioo.Event{
		EmployeeID:  n.EmployeeID,
		CustomerID:  n.CustomerID,
		Title:       n.Title,
		Description: n.Description,
		Start:       time.Unix(intStart, 0),
		End:         time.Unix(intEnd, 0),
		Location:    n.Location,
		Notes:       n.Notes,
		CreatedAt:   time.Now(),
	}

	if err := s.DB.Insert(e); err != nil {
		return nil, err
	}

	return e, nil
}

// Event is
func (s *EventService) Event(id string) (*adioo.Event, error) {
	e := new(adioo.Event)
	if err := s.DB.Model(e).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return e, nil
}

// ListEmployeeEvents is
func (s *EventService) ListEmployeeEvents(employeeID, start, end, sinceID string, limit int) ([]*adioo.Event, error) {
	intStart, err := strconv.ParseInt(start, 10, 64)
	if err != nil {
		return nil, err
	}
	intEnd, err := strconv.ParseInt(end, 10, 64)
	if err != nil {
		return nil, err
	}

	var e []*adioo.Event
	if sinceID != "" {
		sinceEvent, err := s.Event(sinceID)
		if err != nil {
			return nil, err
		}
		if err := s.DB.Model(&e).Where("employee_id = ? and start >= ? and start <= ? and start < ?", employeeID, time.Unix(intStart, 0), time.Unix(intEnd, 0), sinceEvent.Start).Limit(limit).Order("start DESC").Select(); err != nil {
			return nil, err
		}
		return e, nil
	}

	if err := s.DB.Model(&e).Where("employee_id = ? and start >= ? and start <= ?", employeeID, time.Unix(intStart, 0), time.Unix(intEnd, 0)).Limit(limit).Order("start DESC").Select(); err != nil {
		return nil, err
	}
	return e, nil
}

// Count is
func (s *EventService) Count() (int, error) {
	return s.DB.Model(&adioo.Event{}).Count()
}
