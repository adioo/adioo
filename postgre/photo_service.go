package postgre

import (
	"time"

	"gitlab.com/adioo/adioo"
	pg "gopkg.in/pg.v5"
)

// PhotoService is
type PhotoService struct {
	DB *pg.DB
}

// Create is
func (s *PhotoService) Create(n *adioo.NewPhoto) (*adioo.Photo, error) {
	p := &adioo.Photo{
		ProjectID:    n.ProjectID,
		URL:          n.URL,
		ThumbnailURL: n.ThumbnailURL,
		CreatedAt:    time.Now(),
	}

	if err := s.DB.Insert(p); err != nil {
		return nil, err
	}

	return p, nil
}

// AddFolderPhoto is
func (s *PhotoService) AddFolderPhoto(folderID, PhotoID string) error {
	f := &adioo.FolderPhoto{
		FolderID: folderID,
		PhotoID:  PhotoID,
	}

	return s.DB.Insert(f)
}

// ListProjectPhotos is
func (s *PhotoService) ListProjectPhotos(projectID string) ([]*adioo.Photo, error) {
	var p []*adioo.Photo
	if err := s.DB.Model(&p).Where("project_id = ?", projectID).Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}
	return p, nil
}

// RemovePhoto is
func (s *PhotoService) RemovePhoto(id string) error {
	r, err := s.DB.Model(&adioo.Photo{}).Where("id = ?", id).Delete()
	if err != nil {
		if err == pg.ErrNoRows {
			return adioo.ErrNotFound
		}
		return err
	}

	if r.RowsAffected() == 0 {
		return adioo.ErrNotFound
	}

	return nil
}

// Photo is
func (s *PhotoService) Photo(id string) (*adioo.Photo, error) {
	p := new(adioo.Photo)
	if err := s.DB.Model(p).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, adioo.ErrNotFound
		}
		return nil, err
	}

	return p, nil
}

// RemoveFolderPhoto is
func (s *PhotoService) RemoveFolderPhoto(folderID, PhotoID string) error {
	r, err := s.DB.Model(&adioo.FolderPhoto{}).Where("folder_id = ? and photo_id = ?", folderID, PhotoID).Delete()
	if err != nil {
		return err
	}
	if r.RowsAffected() == 0 {
		return adioo.ErrNotFound
	}
	return nil
}
