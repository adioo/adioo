package adioo

import (
	"time"
)

// ProjectService is
type ProjectService interface {
	Create(p *NewProject) (*Project, error)
	Project(id string) (*Project, error)
	ListCompanyProjects(companyID, category string) ([]*Project, error)
	CompanyCategory(companyID, name string) (*ProjectCategory, error)
	CreateCategory(companyID, name, description string) (*ProjectCategory, error)
	Category(id string) (*ProjectCategory, error)
	ListCompanyCategories(companyID string) ([]*ProjectCategory, error)
	Update(u *UpdateProject) (*Project, error)
}

// Project is
type Project struct {
	ID           string    `json:"id"`
	CompanyID    string    `json:"company_id"`
	Name         string    `json:"name"`
	CustomerID   string    `json:"customer_id"`
	Description  string    `json:"description"`
	Location     string    `json:"location"`
	CategoryID   string    `json:"category_id"`
	Folders      []*Folder `json:"folders"`
	Photos       []*Photo  `json:"photos"`
	ThumbnailURL string    `json:"thumbnail_url"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

// ProjectCategory is
type ProjectCategory struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	CompanyID   string    `json:"company_id"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"-"`
	UpdatedAt   time.Time `json:"-"`
}

// NewProject is
type NewProject struct {
	CompanyID    string `json:"company_id"`
	Name         string `json:"name"`
	CustomerID   string `json:"customer_id"`
	Description  string `json:"description"`
	Location     string `json:"location"`
	CategoryID   string `json:"category_id"`
	ThumbnailURL string `json:"thumbnail_url"`
}

// UpdateProject is
type UpdateProject struct {
	ID           string `json:"id"`
	Name         string `json:"name"`
	CustomerID   string `json:"customer_id"`
	Description  string `json:"description"`
	Location     string `json:"location"`
	CategoryID   string `json:"category_id"`
	ThumbnailURL string `json:"thumbnail_url"`
}
