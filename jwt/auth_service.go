package jwt

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// AuthService is
type AuthService struct{}

// Token is
func (s *AuthService) Token(employeeID, companyID string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	scope := &Scopes{}
	claims := token.Claims.(jwt.MapClaims)
	claims["sub"] = employeeID
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Hour * 720).Unix() // Expire in 30 days
	claims["nbf"] = time.Now().Unix()
	claims["iss"] = os.Getenv("BASE_URL")
	claims["jti"] = makeJTI(claims["sub"], claims["iat"])
	claims["scope"] = scope
	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_TOKEN")))
	if err != nil {
		return "", fmt.Errorf("could not sign token string: %v", err)
	}
	return tokenString, nil
}

// Scopes is
type Scopes struct {
	Employee employee `json:"employee"`
	Company  company  `json:"company"`
}

type employee struct {
	Actions []string `json:"actions"`
}

type company struct {
	Actions []string `json:"actions"`
}

func makeJTI(subject, issuedAt interface{}) string {
	h := md5.New()
	io.WriteString(h, subject.(string))
	io.WriteString(h, strconv.Itoa(int(issuedAt.(int64))))
	return hex.EncodeToString(h.Sum(nil))
}
