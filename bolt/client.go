package bolt

import bolt "github.com/coreos/bbolt"

// Open creates and opens a database at the given path.
// If the file does not exist then it will be created automatically.
// Passing in nil options will cause Bolt to open the database with the default options.
func Open(source string) (*bolt.DB, error) {
	return bolt.Open(source, 0600, nil)
}
