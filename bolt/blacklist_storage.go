package bolt

import (
	"log"
	"time"

	"gitlab.com/adioo/adioo"

	bolt "github.com/coreos/bbolt"
)

// BlacklistStorage is
type BlacklistStorage struct {
	DB *bolt.DB
}

// NewStorage is
func NewStorage(db *bolt.DB) (*BlacklistStorage, error) {
	s := &BlacklistStorage{
		DB: db,
	}

	if err := db.Update(func(t *bolt.Tx) error {
		t.CreateBucketIfNotExists([]byte("blacklist"))
		return nil
	}); err != nil {
		return nil, err
	}

	go s.sweep()

	return s, nil
}

// CheckJTI is
func (s *BlacklistStorage) CheckJTI(jti string) (bool, error) {
	ok := false
	if err := s.DB.View(func(t *bolt.Tx) error {
		b := t.Bucket([]byte("blacklist"))
		res := b.Get([]byte(jti))
		if res == nil {
			ok = true
		}
		return nil
	}); err != nil {
		return false, err
	}
	return ok, nil
}

// Create is
func (s *BlacklistStorage) Create(bl *adioo.Blacklist) error {
	by, err := adioo.Bytes(bl)
	if err != nil {
		return err
	}

	return s.DB.Update(func(t *bolt.Tx) error {
		b := t.Bucket([]byte("blacklist"))
		return b.Put([]byte(bl.JTI), by)
	})
}

func (s *BlacklistStorage) sweep() {
	ticker := time.NewTicker(10 * time.Minute)
	for {
		<-ticker.C
		if err := s.DB.View(func(t *bolt.Tx) error {
			b := t.Bucket([]byte("blacklist"))
			b.ForEach(func(k, v []byte) error {
				var bl adioo.Blacklist
				if err := adioo.FromBytes(v, bl); err != nil {
					return err
				}
				if bl.Expires <= time.Now().Unix() {
					return b.Delete(k)
				}
				return nil
			})
			return nil
		}); err != nil {
			log.Println(err)
		}
	}
}
