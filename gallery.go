package adioo

import "time"

// GalleryService is
type GalleryService interface {
	Create(title, employeeID string, photos []string, permissions []string) (*Gallery, error)
	Gallery(id string) (*Gallery, error)
	ListEmployeeGalleries(employeeID string) ([]*Gallery, error)
	ListCompanyGalleries(companyID string) ([]*Gallery, error)
	AddPhoto(galleryID, src string) (*GalleryPhoto, error)
	RemovePhoto(id string) error
	CreatePhotoComment(n *NewGalleryPhotoComment) (*GalleryPhotoComment, error)
	ListPhotoComments(id string) ([]*GalleryPhotoComment, error)
	GetPhoto(id string) (*GalleryPhoto, error)
}

// Gallery is
type Gallery struct {
	ID          string          `json:"id"`
	Title       string          `json:"title"`
	EmployeeID  string          `json:"employee_id"`
	Photos      []*GalleryPhoto `json:"photos"`
	Permissions []*Permission   `json:"permissions" pg:",many2many:gallery_permission"`
	CreatedAt   time.Time       `json:"created_at"`
	UpdatedAt   time.Time       `json:"updated_at"`
}

// GalleryPermission is
type GalleryPermission struct {
	TableName    struct{} `sql:"gallery_permission"`
	GalleryID    string
	PermissionID string
}

// GalleryPhoto is
type GalleryPhoto struct {
	ID              string          `json:"id"`
	GalleryID       string          `json:"gallery_id"`
	Src             string          `json:"url"`
	GalleryPhotoID  string          `json:"parent_id"`
	IsOriginal      bool            `json:"is_original"`
	Transformations []*GalleryPhoto `json:"transormations" pg:"fk:gallery_photo_id"`
	CreatedAt       time.Time       `json:"created_at"`
	UpdatedAt       time.Time       `json:"updated_at"`
}

// GalleryPhotoComment is
type GalleryPhotoComment struct {
	ID         string      `json:"id"`
	PhotoID    string      `json:"photo_id"`
	Text       string      `json:"text"`
	CustomerID string      `json:"customer_id"`
	Anchor     PhotoAnchor `json:"anchor"`
	CreatedAt  time.Time   `json:"created_at"`
	UpdatedAt  time.Time   `json:"updated_at"`
}

// NewGalleryPhotoComment is
type NewGalleryPhotoComment struct {
	PhotoID    string      `json:"photo_id"`
	Text       string      `json:"text"`
	CustomerID string      `json:"customer_id"`
	Anchor     PhotoAnchor `json:"anchor"`
}

// PhotoAnchor is
type PhotoAnchor struct {
	X int `json:"x"`
	Y int `json:"y"`
}
