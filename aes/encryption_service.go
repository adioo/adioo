package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
)

// EncryptionService is
type EncryptionService struct {
}

// EncryptURL is
func (s *EncryptionService) EncryptURL(u string) (string, error) {
	pu, err := url.Parse(u)
	if err != nil {
		return "", err
	}
	fragment := pu.Path + "?" + pu.RawQuery
	cipherText, err := encrypt([]byte(fragment))
	fullURL := "http://" + pu.Host + "/" + hex.EncodeToString(cipherText)
	return fullURL, nil
}

// DecryptURL is
func (s *EncryptionService) DecryptURL(u string) (string, error) {
	parsedURL, err := url.Parse(u)
	if err != nil {
		return "", err
	}
	ciphertext := strings.Trim(parsedURL.Path, "/")

	decodedCipherText, err := hex.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}

	plaintext, err := decrypt(decodedCipherText)
	if err != nil {
		return "", err
	}

	fullURL := fmt.Sprintf("http://%s%s", parsedURL.Host, plaintext)
	return fullURL, err
}

// DecryptURLs is
func (s *EncryptionService) DecryptURLs(us []string) ([]string, error) {
	var decryptedURLs []string
	for _, u := range us {
		du, err := s.DecryptURL(u)
		if err != nil {
			return nil, err
		}
		decryptedURLs = append(decryptedURLs, du)
	}

	return decryptedURLs, nil
}

func decrypt(ciphertext []byte) ([]byte, error) {
	key := []byte(os.Getenv("AES_KEY"))
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	return gcm.Open(nil, nonce, ciphertext, nil)
}

func encrypt(plaintext []byte) ([]byte, error) {
	key := []byte(os.Getenv("AES_KEY"))
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}
