package adioo

import (
	"bytes"
	"encoding/gob"
)

// StringInSlice is
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// FromBytes transform a byte array into an struct
func FromBytes(b []byte, v interface{}) error {
	return gob.NewDecoder(bytes.NewReader(b)).Decode(&v)
}

//Bytes transform what ever payload is past in to a byte array
func Bytes(v interface{}) ([]byte, error) {
	b := new(bytes.Buffer)
	if err := gob.NewEncoder(b).Encode(v); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}
