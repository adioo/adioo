CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "companies" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "name" varchar(255) NOT NULL,
    "phone_number" varchar(15) UNIQUE,
    "email" varchar(255) UNIQUE CHECK (
        CASE 
            WHEN "phone_number" IS NULL 
            AND "email" IS NULL 
            THEN FALSE ELSE TRUE 
        END),
    "address" varchar(255),
    "state" varchar(255),
    "zip_code" integer,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")
);

CREATE TABLE "employees" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "company_id" uuid NOT NULL,
    "first_name" varchar(255) NOT NULL,
    "last_name" varchar(255) NOT NULL,
    "email" varchar(255) NOT NULL UNIQUE,
    "phone_number" varchar(15) UNIQUE,
    "password" varchar(60),
    "jti" varchar(32),
    "avatar_url" varchar(60),
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("company_id") REFERENCES "companies"("id") ON DELETE CASCADE
);

CREATE TABLE "customers" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "company_id" uuid NOT NULL,
    "first_name" varchar(255) NOT NULL,
    "last_name" varchar(255) NOT NULL,
    "email" varchar(255) UNIQUE,
    "phone_number" varchar(15) UNIQUE,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("company_id") REFERENCES "companies"("id") ON DELETE CASCADE
);

CREATE TABLE "project_categories" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "name" varchar(255) NOT NULL,
    "company_id" uuid NOT NULL,
    "description" text,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("company_id") REFERENCES "companies"("id") ON DELETE CASCADE
);

CREATE TABLE "projects" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "company_id" uuid NOT NULL,
    "name" varchar(255) UNIQUE NOT NULL,
    "customer_id" uuid NOT NULL,
    "description" text,
    "category_id" uuid NOT NULL,
    "location" varchar(255),
    "thumnail_url" varchar(255),
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("company_id") REFERENCES "companies"("id") ON DELETE CASCADE, 
    FOREIGN KEY ("customer_id") REFERENCES "customers"("id"),
    FOREIGN KEY ("category_id") REFERENCES "project_categories"("id")
);

CREATE TABLE "employee_project" (
    "employee_id" uuid NOT NULL,
    "project_id" uuid NOT NULL,
    FOREIGN KEY ("employee_id") REFERENCES "employees"("id"),
    FOREIGN KEY ("project_id") REFERENCES "companies"("id")
);

CREATE TABLE "folders" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "project_id" uuid NOT NULL,
    "name" varchar(255) NOT NULL,
    "description" text,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE CASCADE
);

CREATE TABLE "photos" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "project_id" uuid NOT NULL,
    "url" varchar(255) NOT NULL,
    "thumbnail_url" varchar(255),
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE CASCADE,
    PRIMARY KEY ("id")
);

CREATE TABLE "folder_photo" (
    "folder_id" uuid NOT NULL,
    "photo_id" uuid NOT NULL,
    FOREIGN KEY ("folder_id") REFERENCES "folders"("id") ON DELETE CASCADE,
    FOREIGN KEY ("photo_id") REFERENCES "photos"("id") ON DELETE CASCADE
);

CREATE TABLE "permissions" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "name" varchar(255) NOT NULL,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")
);

CREATE TABLE "employee_permission" (
    "employee_id" uuid NOT NULL,
    "permission_id" uuid NOT NULL,
    FOREIGN KEY ("employee_id") REFERENCES "employees"("id") ON DELETE CASCADE,
    FOREIGN KEY ("permission_id") REFERENCES "permissions"("id") ON DELETE CASCADE
);
CREATE INDEX  ON "employee_permission"("employee_id");
CREATE INDEX ON "employee_permission"("permission_id");

CREATE TABLE "events" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "employee_id" uuid NOT NULL,
    "customer_id" uuid,
    "title" varchar(255) NOT NULL,
    "description" text,
    "start" timestamp NOT NULL,
    "end" timestamp NOT NULL,
    "location" varchar(255),
    "notes" text,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("employee_id") REFERENCES "employees"("id") ON DELETE CASCADE,
    FOREIGN KEY ("customer_id") REFERENCES "customers"("id") ON DELETE CASCADE
);

CREATE TABLE "galleries" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "title" varchar(255) NOT NULL,
    "employee_id" uuid NOT NULL,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("employee_id") REFERENCES "employees"("id")
);

CREATE TABLE "gallery_permission" (
    "gallery_id" uuid NOT NULL,
    "permission_id" uuid NOT NULL,
    UNIQUE("gallery_id", "permission_id"),
    FOREIGN KEY ("gallery_id") REFERENCES "galleries"("id") ON DELETE CASCADE,
    FOREIGN KEY ("permission_id") REFERENCES "permissions"("id") ON DELETE CASCADE
);
CREATE INDEX  ON "gallery_permission"("gallery_id");
CREATE INDEX ON "gallery_permission"("permission_id");

CREATE TABLE "gallery_photos" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "gallery_id" uuid NOT NULL,
    "src" text NOT NULL,
    "gallery_photo_id" uuid,
    "is_original" BOOLEAN NOT NULL DEFAULT false,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    UNIQUE("gallery_id", "src"),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("gallery_id") REFERENCES "galleries"("id") ON DELETE CASCADE,
    FOREIGN KEY ("gallery_photo_id") REFERENCES "gallery_photos"("id") ON DELETE CASCADE
);

CREATE TABLE "gallery_photo_comments" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "photo_id" uuid NOT NULL,
    "text" text NOT NULL,
    "customer_id" uuid,
    "anchor" jsonb,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("photo_id") REFERENCES "gallery_photos"("id") ON DELETE CASCADE
);

ALTER TABLE projects ADD constraint unique_company_id_name unique(company_id, name);
ALTER TABLE folders ADD constraint unique_project_id_name unique(project_id, name);
