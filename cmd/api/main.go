package main

import (
	"log"
	"os"

	"gitlab.com/adioo/adioo/aes"
	"gitlab.com/adioo/adioo/http/httpauth"
	"gitlab.com/adioo/adioo/http/httpcompany"
	"gitlab.com/adioo/adioo/http/httpcustomer"
	"gitlab.com/adioo/adioo/http/httpemployee"
	"gitlab.com/adioo/adioo/http/httpevent"
	"gitlab.com/adioo/adioo/http/httpfolder"
	"gitlab.com/adioo/adioo/http/httpgallery"
	"gitlab.com/adioo/adioo/http/httpphoto"
	"gitlab.com/adioo/adioo/http/httpproject"
	"gitlab.com/adioo/adioo/http/middleware"

	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"gitlab.com/adioo/adioo/bolt"
	"gitlab.com/adioo/adioo/http"
	"gitlab.com/adioo/adioo/jwt"
	"gitlab.com/adioo/adioo/postgre"
	pg "gopkg.in/pg.v5"
)

func main() {
	if err := godotenv.Load(".env"); err != nil {
		log.Println("Error loading .env file")
	}

	db := pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USER"),
		Addr:     os.Getenv("DB_ADDR"),
		Database: os.Getenv("DB_NAME"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	defer db.Close()

	projectService := &postgre.ProjectService{
		DB: db,
	}

	folderService := &postgre.FolderService{
		DB: db,
	}

	companyService := &postgre.CompanyService{
		DB: db,
	}

	employeeService := &postgre.EmployeeService{
		DB: db,
	}

	customerService := &postgre.CustomerService{
		DB: db,
	}

	eventService := &postgre.EventService{
		DB: db,
	}

	galleryService := &postgre.GalleryService{
		DB:              db,
		EmployeeStorage: employeeService,
	}

	photoService := &postgre.PhotoService{
		DB: db,
	}

	boltDB, err := bolt.Open("blacklist.db")
	if err != nil {
		log.Fatal(err)
	}

	blacklistStorage, err := bolt.NewStorage(boltDB)
	if err != nil {
		log.Fatal(err)
	}

	h, err := http.NewHandler(&http.HandlerConfig{
		CompanyHandler:  httpcompany.NewHandler(companyService),
		EventHandler:    httpevent.NewHandler(eventService),
		FolderHandler:   httpfolder.NewHandler(folderService, &aes.EncryptionService{}),
		PhotoHandler:    httpphoto.NewHandler(photoService, &aes.EncryptionService{}),
		GalleryHandler:  httpgallery.NewHandler(galleryService, &aes.EncryptionService{}),
		CustomerHandler: httpcustomer.NewHandler(customerService),
		ProjectHandler:  httpproject.NewHandler(projectService),
		EmployeeHandler: httpemployee.NewHandler(employeeService),
		AuthHandler:     httpauth.NewHandler(employeeService, blacklistStorage, &jwt.AuthService{}),
	})
	if err != nil {
		log.Fatal(err)
	}

	opts := cors.Options{
		AllowedMethods: []string{"PUT", "POST", "GET", "DELETE", "PATCH"},
		AllowedHeaders: []string{"Origin", "Accept", "Content-Type", "Authorization"},
	}

	c := cors.New(opts)
	ch := c.Handler(h)
	rc := middleware.GenerateRequestID(ch)
	log.Fatal(http.Serve(rc))
}
