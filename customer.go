package adioo

import (
	"strings"
	"time"
)

// CustomerService is
type CustomerService interface {
	Create(c *NewCustomer) (*Customer, error)
	Customer(id string) (*Customer, error)
	ListCompanyCustomers(companyID, sinceID string, limit int) ([]*Customer, error)
}

// Customer is
type Customer struct {
	ID          string    `json:"id"`
	CompanyID   string    `json:"company_id"`
	Company     *Company  `json:"company"`
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Email       string    `json:"email"`
	PhoneNumber string    `json:"phone_number"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// NewCustomer is
type NewCustomer struct {
	CompanyID   string `json:"company_id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
}

// Sanitize is
func (c *NewCustomer) Sanitize() {
	c.FirstName = strings.ToLower(c.FirstName)
	c.LastName = strings.ToLower(c.LastName)
}
